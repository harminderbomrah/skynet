# unicorn_rails -c config/unicorn.rb -E development -D
# kill -s USR2 PID_OF_MASTER

rails_root  = `pwd`.gsub("\n", "")

rails_env = ENV['RAILS_ENV'] || 'production'

cpu_cores = %x(cat /proc/cpuinfo | grep processor | wc -l).sub("\n",'').to_i rescue 2

worker_processes (rails_env == 'production' ? cpu_cores : 1)

# preload_app true

timeout 30

listen "#{rails_root}/tmp/unicorn.sock", :backlog => 64
stderr_path "#{rails_root}/log/unicorn.log"
stdout_path "#{rails_root}/log/unicorn.log"

before_fork do |server, worker|

  old_pid = "#{rails_root}/tmp/pids/unicorn.pid.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
    end
  end
end
