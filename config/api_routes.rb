get "/employee/:employer_id/search", to: "employer_api#search_employee"
post "/employee/new", to: "employee_api#create_employee"
post "/employee/login", to: "employee_api#login"
patch "/employee/:user_id/profile", to: "employee_api#update_profile"
patch "/employee/:user_id/skill_tags", to: "employee_api#update_skill_tags"
post "/employee/:user_id/education/new", to: "employee_api#create_education"
patch "/employee/:user_id/education/:education_id", to: "employee_api#update_education"
delete "/employee/:user_id/education/:education_id", to: "employee_api#delete_education"
get "/employee/:user_id/profile", to: "employee_api#profile"


get "/general/:state_id/cities", to: "general_api#cities"
get "/general/states", to: "general_api#states"
get "/general/locations", to: "general_api#locations"
get "/general/skills", to: "general_api#skills"
get "/general/qualifications/:type", to: "general_api#qualifications"
get "/general/specializations/:type", to: "general_api#specializations"
get "/general/:qualification_id/specializations/:type", to: "general_api#specializations"
get "/general/designations", to: "general_api#designations"
get "/general/industries", to: "general_api#industries"
get "/general/job/categories", to: "general_api#job_categories"
get "/general/job/:job_category_id/subcategories", to: "general_api#job_sub_categories"

post "/general/forgotpassword", to: "general_api#forgotpassword"

post "/employer/new", to: "employer_api#create_employer"
post "/employer/login", to: "employer_api#login"
post "/employer/:user_id/job/new", to: "employer_api#create_job"
get "/employer/:user_id/jobs", to: "employer_api#job_list"
delete "/employer/:user_id/job/:job_id", to: "employer_api#delete_job"
patch "/employer/:user_id/job/:job_id", to: "employer_api#update_job"


