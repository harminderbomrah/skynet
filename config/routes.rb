Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # site root

  def draw(routes_name)
    instance_eval(File.read(Rails.root.join("config/#{routes_name}.rb")))
  end

  root 'home#index'
  
  get "about_us", to: "home#about_us"
  get "contact_us", to: "home#contact_us"
  get "our_team", to: "home#our_team"
  get "categories", to: "home#categories"
  get "privacy_policy", to: "home#privacy_policy"

  resources :home do 
    collection do 
      get "about_us"
      get "contact_us"
      post "send_mail"
      get "our_team"
      get "categories"
      get "privacy_policy"
    end
  end

  # resources :sessions
  
  #sessions and sign up
  get "/users/login", to: "sessions#new", as: "login"
  get "/users/logout", to: "sessions#destroy", as: "logout"
  get "/users/resetpassword", to: "users#resetpassword", as: "resetpasswordform"
  post "/users/resetpassword", to: "users#resetpassword", as: "resetpassword"
  get "/users/resetp", to: "users#resetp"
  post "/users/updatepassword", to: "users#updatepassword", as: "updatepassword"
  get "/users/confirmation", to: "users#confirmation"
  get "/users/confirm", to: "users#confirm_user_email"
  get "/users/confirm_failure", to: "users#confirm_failure", as: "confirm_failure"
  get "/users/confirm_success", to: "users#confirm_success", as: "confirm_success"

  resources :users
  resources :sessions
  resources :jobs do 
    collection do 
      get "get_job_sub"
    end
    member do 
      get "bookmark"
    end
  end

  resources :resumes do
    member do
      get "bookmark"
    end
  end

  namespace :api do
    namespace :v1 do
      draw :api_routes      
    end
  end


  namespace :member do
    resources :profile do 
      collection do
        get "getselectdata"
      end
      member do 
        get "editresume"
      end
    end
    resources :jobs do 
      collection do
        get "get_job_sub"
        get "get_specialization"
        post "set_review_status"
        post "send_job_letter"
      end
      member do
        get "applications"
        post "application_info"
        get "fill_status"
        get "job_letter"
        delete "delete_job_application"
      end
    end
    resources :dashboards
  end

  namespace :admin do
    resources :dashboards do 
      collection do
        get "get_chart_data"
      end
    end
    resources :admin_profiles
    resources :employers
    resources :jobs do
      collection do
        get "get_job_subad"
        get "get_specializationad"
      end
    end
    resources :employees do
      collection do
        get "getselectdata"
        get "search"
      end
    end
    resources :settings do
      collection do 
        get "job_category_index"
        get "new_job_category"
        post "create_job_category"

        get "job_sub_category_index"
        get "new_job_sub_category"
        post "create_job_sub_category"

        get "ug_qualification_index"
        get "new_ug_qualification"
        post "create_ug_qualification"

        get "pg_qualification_index"
        get "new_pg_qualification"
        post "create_pg_qualification"

        get "phd_qualification_index"
        get "new_phd_qualification"
        post "create_phd_qualification"

        get "industry_index"
        get "new_industry"
        post "create_industry"

        get "country_index"
        get "new_country"
        post "create_country"

        get "location_index"
        get "new_location"
        post "create_location"

        get "designation_index"
        get "new_designation"
        post "create_designation"

      end
      member do
        get "edit_job_category"
        delete "delete_job_category"
        patch "update_job_category"

        get "edit_job_sub_category"
        delete "delete_job_sub_category"
        patch "update_job_sub_category"

        get "edit_ug_qualification"
        get "subjects"
        delete "delete_ug_qualification"
        patch "update_ug_qualification"

        get "edit_pg_qualification"
        get "specializations"
        delete "delete_pg_qualification"
        patch "update_pg_qualification"

        get "edit_phd_qualification"
        get "phd_specializations"
        delete "delete_phd_qualification"
        patch "update_phd_qualification"

        get "edit_industry"
        delete "delete_industry"
        patch "update_industry"

        get "edit_country"
        get "states"
        delete "delete_country"
        patch "update_country"

        get "edit_location"
        delete "delete_location"
        patch "update_location"

        get "edit_designation"
        delete "delete_designation"
        patch "update_designation"

      end
    end
  end

end
