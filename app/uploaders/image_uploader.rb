# encoding: utf-8
module CarrierWave
  module Uploader
    module Versions
      def recreate_version!(version)
        already_cached = cached?
        cache_stored_file! if !already_cached
        send(version).store!
        if !already_cached && @cache_id
          tmp_dir = File.expand_path(File.join(cache_dir, cache_id), CarrierWave.root)
          FileUtils.rm_rf(tmp_dir)
        end
      end
    end
  end
end

class ImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or ImageScience support:
  # include CarrierWave::RMagick
  # include CarrierWave::ImageScience
    include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  # storage :file
  # storage :s3

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def fix_exif_rotation 
    manipulate! do |img|
      img.tap(&:auto_orient)
    end
  end
  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :scale => [50, 50]
  # end
  
  version :thumb do
    process :fix_exif_rotation
    process :resize_to_fill => [200, 200]
  end

  version :thumb_large do
    process :fix_exif_rotation
    process :resize_to_fill => [600, 600]
  end

  version :theater do
    process :fix_exif_rotation
    process :resize_to_limit => [1920, 1080]
  end

  version :mobile do
    process :fix_exif_rotation
    process :resize_to_limit => [1152, 768]
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_white_list
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # def filename
  #   "something.jpg" if original_filename
  # end
  
#  def manipulate!
#    raise current_path.inspect
#    image = ::MiniMagick::Image.open(current_path)
#    image = yield(image)
#    image.write(current_path)
#    ::MiniMagick::Image.open(current_path)
#  rescue ::MiniMagick::Error, ::MiniMagick::Invalid => e
#    raise CarrierWave::ProcessingError.new("Failed to manipulate with MiniMagick, maybe it is not an image? Original Error: #{e}")
#  end
  
end

