class State
	include Mongoid::Document
	include Mongoid::Timestamps

	field :name

	belongs_to :country
	has_many :cities
	
end