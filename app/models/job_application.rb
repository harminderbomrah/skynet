class JobApplication
  include Mongoid::Document
  include Mongoid::Timestamps

  field :cover_letter
  field :note
  field :status, type: Integer, :default => 0
  field :rating, type: Integer
  field :reviewed, type: Boolean, :default => false

  scope :new_applications, ->{where(:status => 0)}
  scope :interviewed, ->{where(:status => 1)}
  scope :offer_extended, ->{where(:status => 2)}
  scope :hired, ->{where(:status => 3)}
  scope :archived, ->{where(:status => 4)}

  belongs_to :employee_profile
  belongs_to :job

  def review_status
    self.reviewed ? "Reviewed" : "Not-Reviewed"
  end

  def rating_for_stars
    ["no","one","two","three","four","five"][self.rating.to_i]
  end

  def get_status
    case self.status
    when 0
      "New"
    when 1
      "Interviewed"
    when 2
      "Offer extended"
    when 3
      "Hired"
    when 4
      "Archived"
    end
  end
end