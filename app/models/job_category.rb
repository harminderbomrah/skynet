class JobCategory
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  field :created_by, type: BSON::ObjectId
  field :icon
  field :show_in_frontend, type: Boolean, :default => false
  scope :homepage_enabled, ->{where(:show_in_frontend => true)}

  has_many :job_sub_categories, dependent: :destroy
  has_many :jobs, dependent: :destroy

  def created_user_name
    User.find(self.created_by).name rescue ""
  end

  def get_icon
    self.icon.blank? ? "ln-icon-Dove" : self.icon
  end

end