class EmployeeProfile
	include Mongoid::Document
	include Mongoid::Timestamps

	mount_uploader :avatar, ImageUploader
	mount_uploader :resume, AssetUploader

	field :name, type: String
	field :gender   # values => male or female
	field :birth_date, type: DateTime
	field :maritial_status	# values => single or married
	field :nationality
	field :professional_title, type: String
	field :video_url
	field :resume_content, type: String
	field :headline, type: String
	field :industry_id
	field :job_category
	field :job_sub_category

	field :contact_phone_number
	field :mobile_number
	field :address
	field :state
	field :location
	field :preferred_location
	field :country
	field :skype_id, type: String
	field :resume_searchable, type: Boolean, default: true
	field :highest_qualification_type, type: Integer, default: 0
	field :total_educations, type: Integer, default: 0
	field :total_urls, type: Integer, default: 0
	field :total_experiences, type: Integer, default: 0
	field :years_of_exp, type: Integer, default: 0
	field :current_ctc_lac, type: Integer
	field :current_ctc_thousand, type: Integer

	field :searchable_text, type: String

	scope :searchable, ->{where(:resume_searchable => true, :resume_content.ne => nil, :name.ne => nil, :contact_phone_number.ne  => nil, :location.ne => nil, :location.ne => "" )}

	has_and_belongs_to_many :skill_tags

	before_save :calculate_exp
	before_save :set_qualification_type
	before_save :set_counts
	before_save :set_serachable_text

	belongs_to :user
	has_many :educations, :autosave => true, :dependent => :destroy
	accepts_nested_attributes_for :educations, :allow_destroy => true

	has_many :experiences, :autosave => true, :dependent => :destroy
	accepts_nested_attributes_for :experiences, :allow_destroy => true

	has_many :urls, :autosave => true, :dependent => :destroy
	accepts_nested_attributes_for :urls, :allow_destroy => true

	has_many :job_applications, :dependent => :destroy

	def set_counts
		self.total_experiences = self.experiences.count
		self.total_urls = self.urls.count
		self.total_educations = self.educations.count
	end

	def calculate_exp
		if self.years_of_exp == 0 || self.years_of_exp.nil?
			total = 0
			self.experiences.each do |exp|
				total = total + exp.get_duration_years
			end
			self.years_of_exp = total
		end
	end

	def set_serachable_text
		rc = pt = nm = hl = ""
		rc = !self.resume_content.nil? ? ActionView::Base.full_sanitizer.sanitize(self.resume_content) : ""
		pt = !self.professional_title.nil? ? self.professional_title : ""
		nm = !self.name.nil? ? self.name : ""
		hl = !self.headline.nil? ? self.headline : ""
		x = (rc + pt + nm + hl).gsub("\n", " ")
		self.searchable_text = x.gsub("\r"," ")
	end

	def get_avatar
		if self.avatar.url.nil?
			return "/assets/avatar-placeholder.png"
		else
			return self.avatar.url
		end
	end

	def set_qualification_type
		self.highest_qualification_type = self.educations.max(:qualification_type)
	end

	def get_highest_qualification
		self.educations.max(:qualification_type)
	end

	def full_nationality
		Country.find(self.nationality).name rescue ""
	end

	def full_location
		ImportantLocation.find(self.location).name rescue ""
	end

	def full_preffered_location
		ImportantLocation.find(self.preferred_location).name rescue ""
	end

	def new_full_location
		City.find(self.location).name rescue ""
	end

	def new_full_preffered_location
		City.find(self.preferred_location).name rescue ""
	end

	def full_country
		Country.find(self.country).name rescue ""
	end

	def full_industry
		Industry.find(self.industry).name rescue ""
	end

	def full_state
		State.find(self.state).name rescue ""
	end

	def ctc_available?
		!(self.current_ctc_lac == nil && self.current_ctc_thousand == nil)
	end

	def get_current_ctc
		s = ""
		if self.current_ctc_lac > 0
			s = s + self.current_ctc_lac.to_s + " Lac(s) "
		end
		if self.current_ctc_thousand > 0
			s = s + self.current_ctc_thousand.to_s + " Thousand(s)"
		end
		s
	end

	def get_latest_exp
		if self.experiences.count == 1
			exp = self.experiences.first
		else
			exp = self.experiences.where(:start_date.ne => nil, :end_date => nil).first rescue nil
			exp = self.experiences.where(:end_date => self.experiences.max(:end_date)).first rescue nil if exp.nil?
		end
		exp
	end
end