class Subject
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  belongs_to :ug_qualification
  
end