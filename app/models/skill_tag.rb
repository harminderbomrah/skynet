class SkillTag
	include Mongoid::Document
	include Mongoid::Timestamps

	field :name, type: String, :localize => true
	field :created_by, type: BSON::ObjectId

	has_and_belongs_to_many :employee_profiles
  has_and_belongs_to_many :jobs
end