class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword
  include Slug

  ADMIN=0
  EMPLOYEE=1
  EMPLOYER=2

  field :user_name, type: String
  field :password_digest, type: String
  field :email
  field :confirmation_token, type: String
  field :reset_token, type: String
  field :user_type, type: Integer, default: 1
  field :basic_registeration_complete, type: Boolean, default: false
  field :super_admin, type: Boolean, default: false

  scope :admins, ->{ where(user_type: self::ADMIN) }
  scope :employers, ->{ where(user_type: self::EMPLOYER) }
  scope :employees, ->{ where(user_type: self::EMPLOYEE) }

  has_secure_password

  has_one :employee_profile, :autosave => true, :dependent => :destroy
  accepts_nested_attributes_for :employee_profile, :allow_destroy => true

  has_one :employer_profile, :autosave => true, :dependent => :destroy
  accepts_nested_attributes_for :employer_profile, :allow_destroy => true
  
  has_one :admin_profile, :autosave => true, :dependent => :destroy
  accepts_nested_attributes_for :admin_profile, :allow_destroy => true

  validates :user_name,format: { with: /\A\w+\Z/ }, uniqueness: true
  validates :email, uniqueness: true
  validates :password, :on => [:create], length: {:in => 8..20}

  def name
    nm = self.user_name
    if self.is_employer?
      nm = !self.employer_profile.nil? && self.employer_profile.company_name != "" && !self.employer_profile.company_name.nil? ? self.employer_profile.company_name : nm
    elsif self.is_employee?
      nm = !self.employee_profile.nil? && self.employee_profile.name != "" && !self.employee_profile.name.nil? ? self.employee_profile.name : nm
    elsif self.is_admin?
       nm = !self.admin_profile.nil? && self.admin_profile.name != "" && !self.admin_profile.name.nil? ? self.admin_profile.name : nm
    end
    nm.split(" ").map(&:capitalize).join(" ")
    # nm
  end

  def avatar
    self.profile.avatar rescue nil
  end

  def registration_status
    return self.basic_registeration_complete ? "Completed" : "Pending"
  end

  def profile
    if self.is_employer?
      self.employer_profile
    elsif self.is_employee?
      self.employee_profile
    elsif self.is_admin?
      self.admin_profile
    end
  end

  def is_premium?
    false
  end

  def is_confirmed?
    self.confirmation_token.nil?
  end

  def is_employer?
    self.user_type == User::EMPLOYER
  end

  def is_employee?
    self.user_type == User::EMPLOYEE
  end

  def is_admin?
    self.user_type == User::ADMIN
  end

  def is_super_admin?
    is_admin? && self.super_admin
  end

  def has_applied?(job)
    self.profile.job_applications.where(:job_id => job.id).count > 0
  end

  def send_confirmation_token(url)
    self.confirmation_token = SecureRandom.hex(5)
    self.save
    ConfirmationMailer.send_confirmation_mail(self, url).deliver
  end

  def send_reset_token(url)
    self.reset_token = SecureRandom.hex(5)
    self.save
    ConfirmationMailer.send_reset_mail(self, url).deliver
  end

  def send_contact_us_mail(name)
    render :text => name and return
  end

  def send_job_letter(msg, employer)
    ConfirmationMailer.job_letter(msg, employer, self).deliver
  end

  def self.confirm_email(confirmation_token = nil)
    if confirmation_token
      user = self.find_by(confirmation_token: confirmation_token) rescue nil
      token_status = user.present?
      case token_status
      when true
        user.confirmation_token = nil
        user.save
        return {:success => "true", :id => user.id.to_s}
      when false
        return {:success => "false"}
      end
    else
        return {:success => "false"}
    end
  end
end
