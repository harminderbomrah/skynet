class EmployerProfile
	include Mongoid::Document
	include Mongoid::Timestamps

	mount_uploader :avatar, ImageUploader

	field :company_name
	field :contact_person
	field :industry
	field :contact_phone_no
	field :company_address
	field :state
	field :location
	field :company_profile
	field :bookmarked_resumes, type: Array, :default => []
	field :call_letter_content

	belongs_to :user
	has_many :jobs, dependent: :destroy

	def full_location
		ImportantLocation.find(self.location).name rescue ""
	end

	def full_industry
		Industry.find(self.industry).name rescue ""
	end

	def full_state
		State.find(self.state).name rescue ""
	end

	def get_avatar
		if self.avatar.url.nil?
			return "/assets/job-list-logo-01.png"
		else
			return self.avatar.url
		end
	end

end