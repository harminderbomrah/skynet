class AdminProfile
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  field :mobile_number

  belongs_to :user
end