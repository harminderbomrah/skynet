class Experience 
	include Mongoid::Document
	include Mongoid::Timestamps

	field :company_name
	field :designation
	field :specialization
	field :start_date, type: DateTime
	field :end_date, type: DateTime
	field :note

	belongs_to :employee_profile


  def get_duration_years
    return 0 if self.start_date.nil?
    sd = self.start_date
    ed = self.end_date.nil? ? DateTime.now : self.end_date
    return (ed.year - sd.year rescue 0)
  end

  def get_duration
    d = ""
    if !self.start_date.nil?
      d = self.start_date.strftime("%B %Y")
    else
      return ""
    end
    if !self.end_date.nil?
      d = d + " ~ " + self.end_date.strftime("%B %Y")
    else
      d = d + " ~ " + "Current"
    end
    return d
  end

end