class Qualification
	include Mongoid::Document
	include Mongoid::Timestamps

	field :name

	belongs_to :education
end