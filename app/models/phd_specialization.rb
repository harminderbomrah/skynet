class PhdSpecialization
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  belongs_to :phd_qualification
  
end

