class Designation
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  field :created_by, type: BSON::ObjectId

  def created_user_name
    User.find(self.created_by).name rescue ""
  end

end