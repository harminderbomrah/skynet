class Industry
	include Mongoid::Document
	include Mongoid::Timestamps

	field :name

  has_many :jobs
end