module Slug
  extend ActiveSupport::Concern

  included do
    field :uid, type: String

    index({ uid: 1}, { unique: true })

    validates_uniqueness_of :uid
    
    before_create :generate_uid
  end

  def to_param
    if self.uid.blank?
      generate_uid
      self.save
    end
    
    (self.slug_title.gsub(/[ "'*@#$%^&()+=;:.,?>|\\\/<~_!：，、。！？；「」〈〉【】／]/,'-')+"-"+self.uid).gsub(/-{2,}/,'-') rescue "-"+self.uid
  end

  def generate_uid
    self.uid = rand(10**8).to_s
    generate_uid if self.class.where(:uid=>self.uid).size > 0
  end
end