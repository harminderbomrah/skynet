class Url
	include Mongoid::Document
	include Mongoid::Timestamps

	field :name
	field :link

	belongs_to :employee_profile

	before_save :add_http

  	protected

	def add_http
		unless /https?:\/\/[\S]+/.match(self.link)
			self.link = "http://#{self.link}"
		end
	end
end