class JobSubCategory
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  field :created_by, type: BSON::ObjectId
  belongs_to :job_category

  has_many :jobs, dependent: :destroy
  
  def created_user_name
    User.find(self.created_by).name rescue ""
  end
  
end