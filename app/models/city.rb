class City
	include Mongoid::Document
	include Mongoid::Timestamps

	field :name

	belongs_to :state
end