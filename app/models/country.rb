class Country
	include Mongoid::Document
	include Mongoid::Timestamps

	field :name

	has_many :states, :autosave => true, :dependent => :destroy
  accepts_nested_attributes_for :states, :allow_destroy => true
end