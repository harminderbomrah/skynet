class Specialization
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  belongs_to :pg_qualification
  
end

