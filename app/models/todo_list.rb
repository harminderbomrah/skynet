class TodoList
  include Mongoid::Document
  include Mongoid::Timestamps

  field :created_by
  field :todo
  field :tagged_users, type: Array, :default => []

end