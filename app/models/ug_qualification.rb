class UgQualification
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  field :created_by, type: BSON::ObjectId

  has_many :subjects, :autosave => true, :dependent => :destroy
  accepts_nested_attributes_for :subjects, :allow_destroy => true

  def created_user_name
    User.find(self.created_by).name rescue ""
  end

end