class Education
	include Mongoid::Document
	include Mongoid::Timestamps

	field :school_name
	field :start_date, type: DateTime
	field :end_date, type: DateTime
	field :note
  field :percentage, type: Integer
  field :qualification_type, type: Integer, default: 0
  field :stream
  field :specialization_id, type: BSON::ObjectId
	field :qualification_id, type: BSON::ObjectId

	belongs_to :employee_profile
	has_one :qualification

  def self.get_qualification_types
    [["HSC", 3], ["SSC", 4], ["Graduation", 0],["Post Graduation", 1], ["PhD",2]]
  end

  def self.get_qualification_type
   ["Graduation", "Post Graduation", "PhD", "HSC", "SSC"][self.qualification_type]
  end

  def get_qualification
    case self.qualification_type
    when 0
      UgQualification.find(self.qualification_id) rescue nil
    when 1
      PgQualification.find(self.qualification_id) rescue nil
    when 2
      PhdQualification.find(self.qualification_id) rescue nil
    end
  end

  def get_specialization
    case self.qualification_type
    when 0
      Subject.find(self.specialization_id) rescue nil
    when 1
      Specialization.find(self.specialization_id) rescue nil
    when 2
      PhdSpecialization.find(self.specialization_id) rescue nil
    end
  end

  def get_qualification_type
    case self.qualification_type
    when 0
      "Graduation"
    when 1
      "Post Graduation"
    when 2
      "PhD"
    end
  end

  def get_duration
    d = ""
    if !self.start_date.nil?
      d = self.start_date.strftime("%B %Y")
    else
      return ""
    end
    if !self.end_date.nil?
      d = d + " ~ " + self.end_date.strftime("%B %Y")
    end
    return d
  end

end