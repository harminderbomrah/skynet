class Job
  include Mongoid::Document
  include Mongoid::Timestamps 
  include Slug

  field :designation_id, type: BSON::ObjectId
  field :job_description
  field :job_type, type:Integer, :default => 0
  field :location_ids, type: Array, :default => []
  field :salary
  field :max_salary
  field :salary_int, type: Integer, :default => 0
  field :max_salary_int, type: Integer, :default => 0
  field :display_salary, type: Boolean, :default => true
  field :min_experience_years, type: Integer, :default => 0
  field :max_experience_years, type: Integer, :default => 0
  field :min_qualification_type, type: Integer, :default => 0
  field :ug_qualification_id
  field :subject_id
  field :pg_qualification_id
  field :specialization_id
  field :phd_qualification_id
  field :phd_specialization_id
  field :filled, type: Boolean, :default => false
  field :sponsored, type: Boolean, :default => false
  field :bookmarked_ids, type: Array, :default => []
  field :premium_job, type: Boolean, :default => false

  scope :not_filled, ->{where(:filled => false)}
  scope :spotlight_jobs, ->{where(:sponsored => true).desc(:max_salary_int).limit(5)}

  before_save :salary_bar
  
  belongs_to :job_category
  belongs_to :job_sub_category
  belongs_to :industry
  belongs_to :employer_profile

  has_many :job_applications, :dependent => :destroy
  has_and_belongs_to_many :skill_tags

  def self.get_qualification_types
    [["Graduation", 0],["Post Graduation", 1], ["PhD",2]]
  end

  def get_qualification_type
    ["Graduation", "Post Graduation", "PhD", "HSC", "SSC"][self.min_qualification_type]
  end

  def self.get_job_types
    [["Full-Time", 0],["Part-Time", 1], ["Internship",2], ["Temporary",3]]
  end

  def salary_bar
    self.salary_int = self.salary.to_i
    self.max_salary_int = self.max_salary.to_i
  end

  def get_job_type
    case self.job_type
    when 0
      "Full-Time"
    when 1
      "Part-Time"
    when 2
      "Internship"
    when 3
      "Temporary"
    end
  end

  def get_locations
    x = self.location_ids
    x.delete("")
    ImportantLocation.find(x).pluck(:name) rescue []
  end

  def is_bookmarked?(id)
    self.bookmarked_ids.include?(id.to_s)
  end

  def is_premium?
    self.premium_job
  end

  def job_profile
    Designation.find(self.designation_id).name rescue ""
  end

  def get_stale_badge
    diff = self.get_stale_period
    if self.filled
      badge = "success"
    elsif diff <= 20
      badge = "warning"
    elsif diff <= 50
      badge = "important"
    else
      badge = "inverse"
    end
    badge
  end

  def get_stale_period
    (Time.now.to_date - self.created_at.to_date).to_i
  end


  def get_salary_in_words
    s = self.salary.sub(",",".")
    ms = self.max_salary.sub(",",".") rescue 0
    s = s.to_f
    ms = ms.to_f
    if ms > s
      x =  s.to_s.sub(".", " Lac(s) ") + " Thousand(s) ~ " + ms.to_s.sub(".", " Lac(s) ") + " Thousand(s)"
      return x.gsub("0 Thousand(s)","")
    else
      x = s.to_s.sub(".", "Lac(s) ") + " Thousand(s)"
      return x.gsub("0 Thousand(s)","")
    end
  end
end


