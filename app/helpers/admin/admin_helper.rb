module Admin::AdminHelper
  def put_active(controller_name=nil)
    @current_controller = controller_name
    if (!controller_name.nil? && "admin/" + controller_name == params[:controller])
      return "open active" 
    end
  end

  def put_sub_active(action_name=nil)
    if !@current_controller.nil? && "admin/" + @current_controller == params[:controller] && action_name == params[:action]
     return "active_2"
    end
  end

  def put_caret_sign
    if !@current_controller.nil? && "admin/" + @current_controller == params[:controller]
      "icon-caret-up"
    else
      "icon-caret-down"
    end
  end

end