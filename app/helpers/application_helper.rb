module ApplicationHelper
  def put_front_active(controller_names=[])
    if (!controller_name.empty? && controller_names.include?(params[:controller]))
      return "id=current" 
    end
  end
end
