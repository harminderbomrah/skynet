class JobsController < FrontendController
	
  def index
    @jobs = []
    sort_type = params[:sort].present? ? params[:sort] : "desc"
    if params[:category].present? && params[:sub_category].present?
      @sub_category = JobSubCategory.find(params[:sub_category]) rescue nil
      @category = @sub_category.job_category if !@sub_category.nil?
      data = @sub_category.jobs.not_filled if !@sub_category.nil?
    elsif params[:category].present?
      @category = JobCategory.find(params[:category]) rescue nil
      data = @category.jobs.not_filled if !@category.nil?
    elsif params[:sub_category].present?
      @sub_category = JobSubCategory.find(params[:sub_category]) rescue nil
      @category = @sub_category.job_category if !@sub_category.nil?
      data = @sub_category.jobs.not_filled if !@sub_category.nil?
    else
      data = Job.not_filled
    end
    data = keyword_search(data)
    data = sort_data(data)
    if !data.blank?
      @jobs = data.order_by(:created_at => sort_type).page(params[:page]).per(10)
    end
	end

  def show
    @job = Job.where(:uid => params[:id]).first rescue nil
    @job_designation = Designation.find(:id => @job.designation_id) rescue nil
    @job_application = JobApplication.new
  end

  def get_job_sub
    jc = JobCategory.find(params[:job_category]) rescue nil
    if !jc.nil?
     @job_sub_categories = jc.job_sub_categories.all.asc(:name)
    end
    render :layout => false
  end

  def bookmark
    job = Job.find(params[:id]) rescue nil
    if params[:status] == "yes"
      job.bookmarked_ids << current_user.id.to_s
    elsif params[:status] == "no"
      job.bookmarked_ids.delete(current_user.id.to_s)
    end
    job.save
    if request.xhr? 
      render :json => {"success" => true}.to_json
    else
      redirect_to member_dashboards_path + "#bookmarks"
    end
  end

  def create
    if !current_user.employee_profile.resume.url.nil?
      job_application = JobApplication.new(job_application_params)
      job_application.employee_profile = current_user.employee_profile
      job_application.save
      render :json => {"success" => true}.to_json
    else
      render :json => {"success" => false, "msg" => "NO_CV_PRESENT"}.to_json
    end
  end

  def destroy
    application = JobApplication.where(:id => params[:id], :employee_profile_id => current_user.profile.id).first rescue nil
    application.destroy if !application.nil?
    redirect_to member_dashboards_path
  end


  private 

  def job_application_params
    params.require(:job_application).permit!
  end

  def keyword_search(data)
    if params[:keywords].present?
      key_string = params[:keywords]
      key_string = key_string.strip.nil? ? key_string : key_string.strip
      keywords = key_string.split(",")
      regex = Regexp.union(keywords.map{|word| Regexp.new(".*" + word.strip + ".*", "i")})
      designation_ids = Designation.where(:name => regex).collect{|d| d.id.to_s}
      employer_ids = EmployerProfile.where(:company_name => regex).collect{|ep| ep.id.to_s}
      skill_tag_ids = SkillTag.where(:name => regex).collect{|st| st.id.to_s}
      query = []
      if !designation_ids.blank?
        query << {:designation_id.in => designation_ids}
      end
      if !employer_ids.blank?
        query << {:employer_profile_id.in => employer_ids}
      end
      if !skill_tag_ids.blank?
        query << {:skill_tag_ids.in => skill_tag_ids}
      end
      if !query.empty?
        data = data.any_of(query)
      end
    end
    data
  end

  def sort_data(data)
    if params[:years].present?
      data = data.where(:min_experience_years.lte => params[:years].to_i)
    end
    if params[:location].present?
      data = data.where(:location_ids.in => params[:location])
    end
    if params[:job_type].present?
      data = data.where(:job_type.in => params[:job_type])
    end
    if params[:salary].present?
      s = params[:salary].split(",")
      min = s.first.to_i
      max = s.last.to_i
      data = data.between(:max_salary_int => min..max)
    end
    data
  end

end