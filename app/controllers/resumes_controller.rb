class ResumesController < FrontendController

  before_action :check_for_employer
  
  def index
    @resumes = []
    sort_type = params[:sort].present? ? params[:sort] : "desc"
    @optimized = false
    if params[:show] == "all"
      @resumes = EmployeeProfile.searchable
    elsif !params[:location].present? && !params[:qualification].present? && !params[:years].present? && !params[:skills].present? && !params[:keywords].present? && !params[:industry].present? && !params[:job_category].present? && !params[:job_sub_category].present?
      if current_user.is_employer?
        jobs = current_user.profile.jobs.not_filled
        if jobs.count > 0
          @optimized = true
          tags = []
          jobs.each do |job|
            tags.concat(job.skill_tags.collect{|st| st.id})
          end
          @resumes = EmployeeProfile.searchable.where(:skill_tag_ids.in => tags)
        else
          @resumes = EmployeeProfile.searchable
        end
      else
        @resumes = EmployeeProfile.searchable
      end
    else
      @resumes = EmployeeProfile.searchable
      @resumes = keyword_search(@resumes)
      @resumes = sort_data(@resumes)
    end
    @resumes = @resumes.order_by(:updated_at => sort_type).page(params[:page]).per(10)
  end

  def show
    @user = User.where(:uid => params[:id]).first rescue nil
    redirect_to resumes_path and return if @user.nil?
    @profile = @user.profile
    if !current_user.is_admin? && !current_user.is_employee?
      @jobs = current_user.profile.jobs.not_filled
    else
      @jobs = []
    end
  end

  def bookmark
    resume = User.where(:uid => params[:id]).first rescue nil
    profile = current_user.profile
    if !resume.nil?
      if params[:status] == "true"
        profile.bookmarked_resumes << resume.uid
      elsif params[:status] == "false"
        profile.bookmarked_resumes.delete(resume.uid)
      end
      profile.save
    end
    render :json => {"success" => true}.to_json
  end

  private 

  def keyword_search(data)
    if params[:keywords].present?
      key_string = params[:keywords]
      key_string = key_string.strip.nil? ? key_string : key_string.strip
      # keywords = key_string.split(/\s+(?=(?:[^"]*"[^"]*")*[^"]*$)/)
      keywords = key_string.split(",")
      # regex = Regexp.union(keywords.map{|word| Regexp.new(".*" + word.strip + ".*", "i")})
      # data = data.any_of({:resume_content => regex},{:professional_title => regex}, {:name => regex}, {:headline => regex})
      regex = Regexp.new(keywords.collect{|word| ".*" + word.strip + ".*"}.join(""), "i")
      data = data.where(:searchable_text => regex)
      #   criteria = criteria + [{:resume_content => regex},{:professional_title => regex}, {:name => regex}, {:headline => regex}]
      # end
      # data = data.any_of(criteria)
    end
    data
  end

  def sort_data(data)
    if params[:years].present?
      if params[:years].to_i > 0
        data = data.where(:years_of_exp.gte => params[:years].to_i)
      else
        data = data.where(:years_of_exp => params[:years].to_i)
      end
    end
    if params[:location].present?
      data = data.where(:location.in => params[:location])
    end
    if params[:qualification].present?
      data = data.where(:highest_qualification_type.gte => params[:qualification])
    end
    if params[:skills].present?
      data = data.where(:skill_tag_ids.in => params[:skills])
    end
    if params[:industry].present?
      data = data.where(:industry_id => params[:industry])
    end
    if params[:job_category].present?
      data = data.where(:job_category => params[:job_category])
    end
    if params[:job_sub_category].present?
      data = data.where(:job_sub_category => params[:job_sub_category])
    end
    data
  end


  def check_for_employer
    redirect_to login_url(:req_path => "#{request.original_fullpath}") and return if current_user.nil? 
    if current_user.is_employee? && current_user.uid != params[:id]
      redirect_to root_url and return 
    end
  end

end

