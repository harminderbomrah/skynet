class UsersController < FrontendController

	def create
		user = User.new(user_params)
		if user.save
			if user.user_type == 1
				profile = EmployeeProfile.new
				profile.user = user
				profile.save
			elsif user.user_type == 2
				profile = EmployerProfile.new
				profile.user = user
				profile.save
			end
			user.send_confirmation_token(request.host_with_port)
			redirect_to users_confirmation_path
		else
			session[:user_creation_fail] = user.errors.full_messages
			redirect_to login_url + "#tab2"
		end
	end

	def confirmation
	end

	def resetpassword
		if request.method == "POST"
			user = User.where(:email => params[:email]).first
			if !user.nil?
      			user.send_reset_token("www.skynetplacements.com")
      			@success = true
      		else
      			@success = false
      		end
		end
	end

	def resetp
		@invalid = true
		@user = User.where(:reset_token => params[:token]).first
		if @user.nil?
			@invalid = false
		end
	end

	def updatepassword
		user = User.find(params[:user_id])
		if !user.nil?
			user.password = params[:password]
			if user.save
				@success = true
			else
				@success = false
			end
		else
			@success = false
		end
	end

	def confirm_user_email
		@user = User.where(:confirmation_token => params[:id]).first rescue nil
		if @user.nil?
			redirect_to confirm_failure_url
		else
			@user.confirmation_token = nil
			@user.save
			redirect_to confirm_success_url
		end
	end

	def confirm_failure
		
	end

	def confirm_success
	end

	private 

	def user_params
		params.require(:user).permit!
	end
	
end