class Api::V1::ApiBaseController < ActionController::Base
	
	protect_from_forgery with: :exception
  helper_method :current_mobile_user
  before_action :check_for_client_token


  def handle_unverified_request
  	if !check_client_token 
      render :json => {"success" => false, "msg" => "Client token mismatch"}.to_json and return
  		raise(ActionController::InvalidAuthenticityToken)
  	end
  end

  def render_invalid_user
    render :json =>{
      "success" => false,
      "msg" => "Invalid user id"
    }.to_json
  end

  def render_invalid_employee
    render :json =>{
      "success" => false,
      "msg" => "User is not employee"
    }.to_json
  end

  	
  private 

  def check_for_client_token
  	if check_client_token == false
  		render :json => {"success" => false, "msg" => "Client token mismatch"}.to_json and return
  	end
  end

  def check_client_token
  	return request.headers["secret-id"] == "iampehlinaukri"
  end
  	
  def current_mobile_user
		@current_mobile_user = @current_mobile_user.nil? ? (User.find(session[:mobile_user_id]) if session[:mobile_user_id] rescue nil) : @current_mobile_user
	end
end