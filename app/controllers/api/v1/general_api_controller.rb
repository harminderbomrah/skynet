class Api::V1::GeneralApiController < Api::V1::ApiBaseController
  def states
    states = State.all.asc(:name).collect do |state|
      {
        "id" => state.id.to_s,
        "name" => state.name
      }
    end
    render :json => {"success" => true, "states" => states}.to_json
  end

  def cities
    state = State.find(params[:state_id]) rescue nil
    if !state.nil?
      cities = state.cities.asc(:name).collect do |city|
        {
          "id" => city.id.to_s,
          "name" => city.name
        }
      end
      data = {"success" => true, "cities" => cities}
    else
      data = {"success" => false, "msg" => "City not found."}
    end
    render :json => data.to_json
  end

  def qualifications
    if ["0","1","2"].include?(params[:type])
      case params[:type]
      when "0"
        qualifications = UgQualification.all.asc(:name).collect{|qual| {"name" => qual.name, "id" => qual.id.to_s}}
      when "1"
        qualifications = PgQualification.all.asc(:name).collect{|qual| {"name" => qual.name, "id" => qual.id.to_s}}
      when "2"
        qualifications = PhdQualification.all.asc(:name).collect{|qual| {"name" => qual.name, "id" => qual.id.to_s}}
      end
      data = {"success" => true, "qualifications" => qualifications}
    else
      data = {"success" => false, "error" => "Invalid type"}
    end
    render :json => data.to_json
  end

  def specializations
    if ["0","1","2"].include?(params[:type])
      case params[:type]
      when "0"
        if params[:qualification_id].present?
          q = UgQualification.find(params[:qualification_id]) rescue nil
        else
          q = UgQualification.first
        end
        if !q.nil?
          specializations = q.subjects.where(:name.ne => "").asc(:name).collect{|qual| {"name" => qual.name, "id" => qual.id.to_s}}
        else
          data = {"success" => false, "msg" => "Invalid UG Qualification"}
        end
      when "1"
        if params[:qualification_id].present?
          q = PgQualification.find(params[:qualification_id]) rescue nil
        else
          q = PgQualification.first
        end
        if !q.nil?
          specializations = q.specializations.where(:name.ne => "").asc(:name).collect{|qual| {"name" => qual.name, "id" => qual.id.to_s}}
        else
          data = {"success" => false, "msg" => "Invalid PG Qualification"}
        end 
      when "2"
        if params[:qualification_id].present?
          q = PhdQualification.find(params[:qualification_id]) rescue nil
        else
          q = PhdQualification.first
        end
        if !q.nil?
          specializations = q.phd_specializations.where(:name.ne => "").asc(:name).collect{|qual| {"name" => qual.name, "id" => qual.id.to_s}}
        else
          data = {"success" => false, "msg" => "Invalid PHD Qualification"}
        end 
      end
      if !specializations.nil?
        data = {"success" => true, "specializations" => specializations}
      end
    else
      data = {"success" => false, "msg" => "Invalid type"}
    end
    render :json => data.to_json
  end

  def forgotpassword
    user = User.where(:email => params[:email]).first rescue nil
    if !user.nil?
      user.send_reset_token("www.skynetplacements.com")
      data = {"success" => true, "msg" => "Reset token sent"}
    else
      data = {"success" => false, "msg" => "User not found"}
    end
    render :json => data.to_json
  end

  def locations
    locations = ImportantLocation.all.collect do |loc|
      {
        "id" => loc.id.to_s,
        "name" => loc.name
      }
    end
    render :json => {"success" => true, "locations" => locations}.to_json
  end

  def skills
    skills = SkillTag.all.asc(:name).collect do |skill|
      {
        "id" => skill.id.to_s,
        "name" => skill.name.strip
      }
    end
    render :json => {"success" => true, "skills" => skills}.to_json
  end

  def designations
    designations = Designation.all.asc(:name).collect do |des|
      {
        "id" => des.id.to_s,
        "title" => des.name
      }
    end
    render :json => {"success" => true, "designations" => designations}.to_json
  end

  def industries
    industries = Industry.all.asc(:name).collect do |des|
      {
        "id" => des.id.to_s,
        "title" => des.name
      }
    end
    render :json => {"success" => true, "industries" => industries}.to_json
  end

  def job_categories
    categories = JobCategory.all.asc(:name).collect do |cat|
      {
        "id" => cat.id.to_s,
        "title" => cat.name
      }
    end
    render :json => {"success" => true, "categories" => categories}.to_json
  end

  def job_sub_categories
    if params[:job_category_id] == "first"
      categories = JobCategory.all.asc(:name).first.job_sub_categories.asc(:name)
    else
      categories = JobCategory.find(params[:job_category_id]).job_sub_categories.asc(:name)
    end
    categories = categories.collect do |cat|
      {
        "id" => cat.id.to_s,
        "title" => cat.name
      }
    end
    render :json => {"success" => true, "sub_categories" => categories}.to_json
  end

end











