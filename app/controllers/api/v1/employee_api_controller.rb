class Api::V1::EmployeeApiController < Api::V1::ApiBaseController

  before_action :check_employee, :except => [:login]
	
	def login
    user = User.find_by(user_name: params[:username]) rescue nil
    if user.nil?
		  user = User.find_by(email: params[:email]) rescue nil
    end
    if (user && user.authenticate(params[:password]) && user.is_employee?)
    	data = {
    		"success" => true,
        "msg" => "You have successfully logged in.",
    		"data" => {
    			"id" => user.id.to_s,
          "uid" => user.uid,
    			"full_name" => user.name,
    			"mobile_no" => user.profile.mobile_number,
    			"email" => user.email,
          "dob" => (user.profile.birth_date.strftime("%Y-%m-%d") rescue ""),
    			"user_type" => user.user_type,
          "avatar" => (user.avatar.url rescue "")
    		}
    	}
		  render :json => data.to_json
		else
			render :json => {
        "success" => false,
        "msg" => "Invalid username password."
      }.to_json
		end
	end

  def create_employee
    user = User.new
    user.user_name = params[:user_name]
    user.password = params[:password]
    user.email = params[:email]
    user.user_type = 1
    if user.save
      user.send_confirmation_token("www.skynetplacements.com")
      profile = EmployeeProfile.new
      profile.name = params[:name] 
      profile.contact_phone_number = params[:contact_phone_number]
      profile.user = user
      profile.save
      render :json => {"success" => true, "msg" => "Registeration Successful"}
    else
      render :json => {"success" => false, "errors" => user.errors.full_messages}
    end
  end

  def profile
    profile = @user.profile
    data = {}
    data["personal"] = {
      "name" => profile.name,
      "mobile_number" => profile.mobile_number,
      "email" => @user.email,
      "dob" => (profile.birth_date.strftime("%d-%m-%Y") rescue ""),
      "address" => profile.address,
      "location" => profile.new_full_location,
      "state" => profile.full_state,
      "preferred_location" => profile.new_full_preffered_location,
      "preferred_location_state" => (City.find(profile.preferred_location).state.name rescue ""),
      "country" => profile.full_country,
      "avatar" => (@user.avatar.url rescue "")

    }

    data["education"] = profile.educations.collect do |edu|
      {
        "id" => edu.id.to_s,
        "type" => edu.get_qualification_type,
        "duration" => edu.get_duration,
        "start_date" => (edu.start_date.strftime("%d/%m/%Y") rescue ""),
        "end_date" => (edu.end_date.strftime("%d/%m/%Y") rescue ""),
        "school_name" => edu.school_name,
        "note" => edu.note,
        "degree" => (edu.get_qualification.name rescue ""),
        "specialization" => (edu.get_specialization.name rescue "")
      }
    end
    data["skills"] = profile.skill_tags.collect{|skill| {"id" => skill.id.to_s, "name" => skill.name.strip} }
    
    counter = 0
    
    data["personal"].keys.each do |fd|
      if !data["personal"][fd].blank?
        counter = counter + 1
      end
    end

    counter += 1 if !data["education"].blank?
    counter += 1 if !data["skills"].blank?

    data["percentage"] = (counter * 100) / (data["personal"].keys.length + 2)
    data["success"] = true

    render :json => data.to_json
  end

  def update_profile
    user = @user rescue nil
    profile = user.profile
    profile.name = params[:name] if params[:name].present?
    profile.mobile_number = params[:mobile_no] if params[:mobile_no].present?
    profile.birth_date = params[:birth_date] if params[:birth_date].present?
    profile.address = params[:address] if params[:address].present?
    profile.avatar = params[:avatar] if params[:avatar].present?
    profile.resume = params[:resume] if params[:resume].present?
    profile.state = params[:state] if params[:state].present?
    profile.location = params[:location] if params[:location].present?
    profile.preferred_location = params[:preferred_location] if params[:preferred_location].present?
    profile.save
    user.save
    render :json => {
      "success" => true,
      "msg" => "Profile updated.",
      "avatar" => (@user.avatar.url rescue "")
    }.to_json

  end

  def update_skill_tags
    profile = @user.profile
    skill_tags = params["skill_tags"]
    if params["new_skill_tags"].present?
      params["new_skill_tags"].each do |nst|
        if nst.length < 41
          tag = SkillTag.create(:name => nst, :created_by => @user.id)
          if skill_tags.present?
            skill_tags << tag.id.to_s
          else
            skill_tags = []
            skill_tags << tag.id.to_s
          end
        end
      end
    end
    profile.skill_tag_ids = skill_tags
    profile.save
    render :json => {
      "success" => true,
      "msg" => "Tags updated."
    }.to_json
  end

  def delete_education
    edu = Education.find(params[:education_id])
    if !edu.nil?
      edu.destroy
      render :json => {
        "success" => true,
        "msg" => "Education deleted."
      }.to_json
    else
      render :json => {
        "success" => false,
        "msg" => "Education not found."
      }.to_json
    end
  end

  def create_education
    profile = @user.profile rescue nil

    education = Education.new
    education.school_name = params[:school_name]
    education.start_date = params[:start_date]
    education.end_date = params[:end_date]
    education.note = params[:note]
    education.qualification_type = params[:qualification_type]
    if ["0","1","2"].include?(params[:qualification_type])
      education.specialization_id = params[:specialization_id]
      education.qualification_id = params[:qualification_id]
    elsif ["3","4"].include?(params[:qualification_type])
      education.stream = params["stream"]
    end
    education.percentage = params["percentage"]
    education.employee_profile = profile
    education.save

    profile.educations << education
    profile.save

    render :json => {
      "success" => true,
      "msg" => "Education succcessfully added"
    }.to_json

  end

  def update_education
    education = Education.find(params[:education_id])
    
    if !education.nil?

      education.school_name = params[:school_name]
      education.start_date = params[:start_date]
      education.end_date = params[:end_date]
      education.note = params[:note]
      education.qualification_type = params[:qualification_type]
      if ["0","1","2"].include?(params[:qualification_type])
        education.specialization_id = params[:specialization_id]
        education.qualification_id = params[:qualification_id]
      elsif ["3","4"].include?(params[:qualification_type])
        education.stream = params[:stream]
      end
      education.percentage = params[:percentage]
     
      if education.save
        render :json => {
          "success" => true,
          "msg" => "Education succcessfully updated."
        }.to_json
      else
        render :json => {
          "success" => false,
          "errors" => education.errors.full_messages
        }.to_json
      end
    else
      render :json => {
        "success" => false,
        "msg" => "Invalid education ID."
      }.to_json
    end

  end

  private

  def check_employee
    if params[:user_id].present?
      @user = User.find(params[:user_id]) rescue nil
      render_invalid_user and return if @user.nil?
      render_invalid_employee and return if !@user.is_employee?
    end
  end


end