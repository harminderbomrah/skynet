class Api::V1::EmployerApiController < Api::V1::ApiBaseController
  include ActionView::Helpers::TextHelper
	
  def create_employer
		user = User.new
		user.user_name = params[:user_name]
		user.password = params[:password]
		user.email = params[:email]
		user.user_type = 2
		if user.save
			user.send_confirmation_token("www.skynetplacements.com")
			profile = EmployerProfile.new
			profile.company_name = params[:company_name] 
			profile.contact_person = params[:contact_person]
			profile.contact_phone_no = params[:contact_phone_no]
			profile.user = user
			profile.save
			render :json => {"success" => true, "msg" => "Registeration Successful"}
		else
			render :json => {"success" => false, "errors" => user.errors.full_messages}
		end
	end

  def login
    user = User.find_by(user_name: params[:username]) rescue nil
    if user.nil?
      user = User.find_by(email: params[:email]) rescue nil
    end
    if (user && user.authenticate(params[:password]) && user.is_employer?)
      profile = user.profile
      data = {
        "success" => true,
        "msg" => "You have successfully logged in.",
        "data" => {
          "id" => user.id.to_s,
          "uid" => user.uid,
          "full_name" => user.name,
          "mobile_no" => profile.contact_phone_no,
          "email" => user.email,
          "dob" => (profile.birth_date.strftime("%Y-%m-%d") rescue ""),
          "user_type" => user.user_type,
          "avatar" => (user.avatar.url rescue "")
        }
      }
      jobs = profile.jobs.collect do |job|
        {
          "id" => job.id.to_s,
          "title" => job.job_profile
        }
      end
      data["jobs"] = jobs
      render :json => data.to_json
    else
      render :json => {
        "success" => false,
        "msg" => "Invalid username password."
      }.to_json
    end
  end

  def job_list
    employer = User.find(params[:user_id]).profile rescue nil
    if !employer.nil? || !employer.is_employer?
      jobs = employer.jobs.collect do |job|
        location_ids = job.location_ids
        location_ids.delete("")
        cities = City.find(location_ids) rescue []

        {
          "id" => job.id.to_s,
          "designation" => job.job_profile,
          "designation_id" => job.designation_id.to_s,
          "job_description" => job.job_description,
          "industry" => job.industry.name,
          "industry_id" => job.industry_id.to_s,
          "skills" => job.skill_tags.collect{|tag| {"id" => tag.id.to_s, "name" => tag.name} },
          "locations" => cities.collect{|location| {"id" => location.id.to_s, "name" => location.name, "state" => location.state.name, "state_id" => location.state.id.to_s}},
          "salary" => job.get_salary_in_words,
          "qualification" => job.get_qualification_type,
          "job_category" => job.job_category.name,
          "job_category_id" => job.job_category.id.to_s,
          "job_sub_category" => job.job_sub_category.name,
          "job_sub_category_id" => job.job_sub_category.id.to_s,
        }
      end
      data = {
        "success" => true,
        "jobs" => jobs
      }
    else
      data = {
        "success" => false,
        "msg" => "Invalid user id."
      }
    end
    render :json => data.to_json
  end

  def delete_job
    job = Job.find(params[:job_id])
    if !job.nil?
      job.destroy
      data = {
        "success" => true,
        "msg" => "Job deleted successfully."
      }
    else
      data = {
        "success" => false,
        "msg" => "Job not found."
      }
    end
    render :json => data.to_json
  end

	def search_employee
    resumes = []
    sort_type = params[:sort].present? ? params[:sort] : "desc"
    user = User.find(params[:user_id]) rescue nil
    optimized = false
    if params[:show] == "all"
      resumes = EmployeeProfile.searchable
    elsif !params[:location].present? && !params[:qualification].present? && !params[:years].present? && !params[:skills].present? && !params[:keywords].present? && !params[:industry].present? && !params[:job_category].present? && !params[:job_sub_category].present?
      if !user.nil? && user.is_employer?
        jobs = current_user.profile.jobs.not_filled
        if jobs.count > 0
          optimized = true
          tags = []
          jobs.each do |job|
            tags.concat(job.skill_tags.collect{|st| st.id})
          end
          resumes = EmployeeProfile.searchable.where(:skill_tag_ids.in => tags)
        else
          resumes = EmployeeProfile.searchable
        end
      else
        resumes = EmployeeProfile.searchable
      end
    else
      resumes = EmployeeProfile.searchable
      resumes = keyword_search(resumes)
      resumes = sort_data(resumes)
    end
    resumes = resumes.order_by(:updated_at => sort_type).page(params[:page]).per(10)
    data = {
    	"resumes" => []
    }
    
    data["resumes"] = []

    resumes.each do |resume|
    	d = {
        "id" => resume.id.to_s,
	    	"name" => resume.name,
				"gender" => resume.gender,
				"birth_date" => (resume.birth_date.strftime("%d/%m/%Y") rescue nil),
				"maritial_status" => resume.maritial_status,
				"nationality" => resume.nationality,
				"professional_title" => resume.professional_title,
				"video_url" => resume.video_url,
				"resume_content" => ActionView::Base.full_sanitizer.sanitize(resume.resume_content),
				"headline" => resume.headline,
				"contact_phone_number" => resume.contact_phone_number,
				"mobile_number" => resume.mobile_number,
				"address" => resume.address,
				"state" => resume.state,
				"location" => resume.location,
				"preferred_location" => resume.preferred_location,
				"country" => resume.country,
				"skype_id" => resume.skype_id,
				"industry" => resume.full_industry,
				"highest_qualification_type" => resume.get_highest_qualification,
				"years_of_exp" => resume.years_of_exp,
				"current_ctc_lac" => resume.current_ctc_lac,
				"current_ctc_thousand" => resume.current_ctc_thousand,
        "avatar" => (resume.avatar.url rescue ""),
        "resume" => (resume.resume.url rescue "")
			}
			d["educations"] = []
			resume.educations.each do |education|
				d["educations"] << {
					"qualification_type" => education.get_qualification_type,
					"school_name" => education.school_name,
					"start_date" => (education.start_date.strftime("%d/%m/%Y") rescue nil),
					"end_date" => (education.end_date.strftime("%d/%m/%Y") rescue nil),
					"note" => education.note,
					"specialization" => (education.get_specialization.name rescue nil),
					"qualification" => (education.get_qualification.name rescue nil)
				}
			end
			d["experiences"] = []
			resume.experiences.each do |experience|
				d["experiences"] << {
					"company_name" => experience.company_name,
					"designation" => experience.designation,
					"specialization" => experience.specialization,
					"start_date" => (experience.start_date.strftime("%d/%m/%Y") rescue nil),
					"end_date" => (experience.end_date.strftime("%d/%m/%Y") rescue nil),
					"note" => experience.note
				}
			end
			d["urls"] = []
			resume.urls.each do |experience|
				d["urls"] << {
					"name" => experience.name,
					"title" => experience.link
				}
			end
    	data["resumes"] << d
    end
    data["success"] = true
    data["optimized"] = optimized
    data["total_pages"] = resumes.total_pages
    render :json => data.to_json
  end

  def create_job
    employer = User.find(params[:user_id]).profile rescue nil

    if employer.nil?
      data = {
        "success" => false,
        "msg" => "Invalid employer id."
      }
    else
      job = Job.new
      job.designation_id = params[:designation_id]
      job.job_description = params[:job_description]
      job.industry_id = params[:industry_id]
      job.location_ids = params[:location_ids]
      job.salary = params[:salary]
      job.min_qualification_type = params[:min_qualification_type]
      job.employer_profile = employer
      job.job_category_id = params[:job_category_id]
      job.job_sub_category_id = params[:job_sub_category_id]
      if params["new_skill_tags"].present?
        params["new_skill_tags"].each do |nst|
          if nst.length < 41
            tag = SkillTag.create(:name => nst, :created_by => employer.user.id)
            if params["skill_tag_ids"].present?
              params["skill_tag_ids"] << tag.id.to_s
            else
              params["skill_tag_ids"] = []
              params["skill_tag_ids"] << tag.id.to_s
            end
          end
        end
      end
      job.skill_tag_ids = params["skill_tag_ids"]
      if job.save
        data = {
          "success" => true,
          "msg" => "Job created successfully"
        }
      else
        data = {
          "success" => false,
          "errors" => job.errors.full_messages
        }
      end
    end
   
    render :json => data.to_json
  end

  def update_job
    employer = User.find(params[:user_id]).profile rescue nil

    if employer.nil?
      data = {
        "success" => false,
        "msg" => "Invalid employer id."
      }
    else
      job = Job.find(params[:job_id])
      job.designation_id = params[:designation_id] if params[:designation_id].present?
      job.job_description = params[:job_description] if params[:job_description].present?
      job.industry_id = params[:industry_id] if params[:industry_id].present?
      job.location_ids = params[:location_ids] if params[:location_ids].present?
      job.salary = params[:salary] if params[:salary].present?
      job.min_qualification_type = params[:min_qualification_type] if params[:min_qualification_type].present?
      job.employer_profile = employer
      job.job_category_id = params[:job_category_id] if params[:job_category_id].present?
      job.job_sub_category_id = params[:job_sub_category_id] if params[:job_sub_category_id].present?
      if params["new_skill_tags"].present?
        params["new_skill_tags"].each do |nst|
          if nst.length < 41
            tag = SkillTag.create(:name => nst, :created_by => employer.user.id)
            if params["skill_tag_ids"].present?
              params["skill_tag_ids"] << tag.id.to_s
            else
              params["skill_tag_ids"] = []
              params["skill_tag_ids"] << tag.id.to_s
            end
          end
        end
      end
      job.skill_tag_ids = params["skill_tag_ids"]
      if job.save
        data = {
          "success" => true,
          "msg" => "Job updated successfully"
        }
      else
        data = {
          "success" => false,
          "errors" => job.errors.full_messages
        }
      end
    end
   
    render :json => data.to_json
  end

  private 

  def keyword_search(data)
    if params[:keywords].present?
      key_string = params[:keywords]
      key_string = key_string.strip.nil? ? key_string : key_string.strip
      # keywords = key_string.split(/\s+(?=(?:[^"]*"[^"]*")*[^"]*$)/)
      keywords = key_string.split(",")
      # regex = Regexp.union(keywords.map{|word| Regexp.new(".*" + word.strip + ".*", "i")})
      # data = data.any_of({:resume_content => regex},{:professional_title => regex}, {:name => regex}, {:headline => regex})
      regex = Regexp.new(keywords.collect{|word| ".*" + word.strip + ".*"}.join(""), "i")
      data = data.where(:searchable_text => regex)
      #   criteria = criteria + [{:resume_content => regex},{:professional_title => regex}, {:name => regex}, {:headline => regex}]
      # end
      # data = data.any_of(criteria)
    end
    data
  end

  def sort_data(data)
    if params[:years].present?
      if params[:years].to_i > 0
        data = data.where(:years_of_exp.gte => params[:years].to_i)
      else
        data = data.where(:years_of_exp => params[:years].to_i)
      end
    end
    if params[:location].present?
      data = data.where(:location.in => params[:location])
    end
    if params[:qualification].present?
      data = data.where(:highest_qualification_type.gte => params[:qualification])
    end
    if params[:skills].present?
      data = data.where(:skill_tag_ids.in => params[:skills])
    end
    if params[:industry].present?
      data = data.where(:industry_id => params[:industry])
    end
    if params[:job_category].present?
      data = data.where(:job_category => params[:job_category])
    end
    if params[:job_sub_category].present?
      data = data.where(:job_sub_category => params[:job_sub_category])
    end
    data
  end

end