class Api::ApiBaseController < ActionController::Base
	
	protect_from_forgery with: :exception
  helper_method :current_mobile_user
  before_action :check_for_client_token


  def handle_unverified_request
  	if check_client_token

  	else
  		raise(ActionController::InvalidAuthenticityToken)
  	end
  end

  	
  private 

  def check_for_client_token
  	if check_client_token == false
  		# render :json => {"success" => false, "msg" => "Client token mismatch"}.to_json and return
  		raise(ActionController::InvalidAuthenticityToken)
  	end
  end

  def check_client_token
  	return true
  end
  	
  def current_mobile_user
		@current_mobile_user = @current_mobile_user.nil? ? (User.find(session[:mobile_user_id]) if session[:mobile_user_id] rescue nil) : @current_mobile_user
	end
end