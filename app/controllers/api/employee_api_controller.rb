class Api::EmployeeApiController < Api::ApiBaseController
	
	def login
		user = User.find_by(user_name: params[:username]) rescue nil
    if (user && user.authenticate(params[:password]) && !user.is_admin?)
    	data = {
    		"success" => true,
    		"data" => {
    			"id" => user.id.to_s,
    			"full_name" => user.name,
    			"mobile_no" => user.profile.contact_phone_number,
    			"email" => user.email,
    			"user_type" => user.user_type 
    		}
    	}
			render :json => data.to_json
		else
			render :json => {"success" => false}.to_json
		end
	end

end