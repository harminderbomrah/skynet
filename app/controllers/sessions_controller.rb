class SessionsController < FrontendController
	
	def new
		@creation_errors = []
		if session[:user_id]
			redirect_to root_url
		end
		if session[:invalid] == true
			@invalid = true
			session.delete(:invalid)
		elsif session[:not_approved] == true
			@not_approved = true
			session.delete(:not_approved)
		elsif session[:user_creation_fail]
			@creation_failure = true
			@creation_errors = session[:user_creation_fail]
			session.delete(:user_creation_fail)
		end
		@user = User.new
	end

	def destroy
		session.delete(:user_id)
		redirect_to root_url
	end

	def create
		user = User.find_by(user_name: params[:username]) rescue nil
		if user.nil?
		  user = User.where(email: params[:email]).first rescue nil
    	end
	    if (user && user.authenticate(params[:password]))
	    	if user.is_confirmed?
		        session[:user_id] = user.id.to_s
		        redirect_to admin_dashboards_path and return if user.is_admin?
		        redirect_to edit_member_profile_path(user.user_name) and return if !user.basic_registeration_complete
			      redirect_to editresume_member_profile_path(user.user_name) and return if user.is_employee? && user.profile.resume.url.nil?
		        if params[:req_path]
		          redirect_to URI.parse(params[:req_path]).path
		        else
		          redirect_to root_url
		        end
		    else
		    	session[:not_approved] = true
	    		redirect_to login_path
		    end
	    else
	    	session[:invalid] = true
	    	redirect_to login_path
	    end
	end

	def destroy
		session[:user_id] = nil
    	redirect_to root_url
	end
end