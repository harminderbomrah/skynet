class SkynetMemberController < ApplicationController
	before_action :check_for_login
	layout "home"
	private

	def check_for_login
		redirect_to login_path(:req_path => "#{request.original_fullpath}") if current_user.nil?
	end

  def check_for_employer
    redirect_to member_dashboards_path and return if !current_user.is_employer?
    if params[:action] != "new"
      @job = Job.where(:uid => params[:id]).first rescue nil
      redirect_to member_dashboards_path and return if @job.nil?
      redirect_to member_dashboards_path and return if @job.employer_profile.id.to_s != current_user.profile.id.to_s
    end
  end

end