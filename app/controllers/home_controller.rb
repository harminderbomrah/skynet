class HomeController < FrontendController
  
  def index
    @recent_jobs = Job.desc(:created_at).limit(6)
    @popular_categories = JobCategory.homepage_enabled
    @spotlight_jobs = Job.not_filled.spotlight_jobs
    if @spotlight_jobs.count == 0
      @spotlight_jobs = Job.not_filled.where(:max_salary_int.gt => 5).limit(5).desc(:max_salary_int)
    end
  end

  def about_us
  end

  def contact_us
  end

  def our_team
  end

  def privacy_policy
  end

  def send_mail
    render :text => params.to_json and return
  end

  def categories
    @categories = JobCategory.all.asc(:name)
  end

end