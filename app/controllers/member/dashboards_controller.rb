class Member::DashboardsController < SkynetMemberController
  
  def index
    if current_user.is_employee?
      @applications = current_user.profile.job_applications.desc(:created_at)
    elsif current_user.is_employer?
      @jobs = current_user.profile.jobs.desc(:created_at)
    end
 
    @bookmarks = Job.where(:bookmarked_ids.in => [current_user.id.to_s])
    @alerts = []
  end

end