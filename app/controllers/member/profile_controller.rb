class Member::ProfileController < SkynetMemberController

	before_action :check_for_username, :only => [:show, :edit, :editresume]

	def index 
	end

	def show
	end

	def editresume
		redirect_to edit_member_profile_path(current_user.user_name) if current_user.is_employer?
		@profile = current_user.profile
		@tag_array = []
		@tag_array_with_ids = {}
		SkillTag.each do |tag|
			@tag_array << tag.name
			@tag_array_with_ids[tag.name] = tag.id.to_s 
		end
	end

	def getselectdata
    if params[:type] == "qualification"
      case params[:value]
      when "0"
        @qualifications = UgQualification.all.asc(:name).collect{|qual| [qual.name, qual.id]}
      when "1"
        @qualifications = PgQualification.all.asc(:name).collect{|qual| [qual.name, qual.id]}
      when "2"
        @qualifications = PhdQualification.all.asc(:name).collect{|qual| [qual.name, qual.id]}
      end
    elsif params[:type] == "specialization"
      case params[:qualification_type]
      when "0"
        @qualifications = UgQualification.find(params[:value]).subjects.asc(:name).collect{|qual| [qual.name, qual.id]}
      when "1"
        @qualifications = PgQualification.find(params[:value]).specializations.asc(:name).collect{|qual| [qual.name, qual.id]}
      when "2"
        @qualifications = PhdQualification.find(params[:value]).phd_specializations.asc(:name).collect{|qual| [qual.name, qual.id]}
      end
    end
    render :layout => false
  end

	def edit
		@profile = current_user.profile
		@industries = Industry.all.asc(:name).collect{|industry| [industry.name, industry.id]}
		@states = Country.first.states.collect{|state| [state.name, state.id]}
		@locations = ImportantLocation.all.collect{|loc| [loc.name, loc.id]}
		@countries = Country.all.collect{|country| [country.name, country.id]}
    @job_categories = JobCategory.all.asc(:name)
    if current_user.is_employee?
      if @profile.job_category.nil? || @profile.job_category.empty?
        @job_sub_categories = @job_categories.first.job_sub_categories.all.asc(:name)
      else
        @job_sub_categories = @job_categories.find(@profile.job_category).job_sub_categories.all.asc(:name)
      end
    end
    
		if current_user.is_employee?
		 @ug_qualifications = UgQualification.all.asc(:name).collect{|qual| [qual.name, qual.id]}
   		 @ug_specializations = UgQualification.find(@ug_qualifications.first.last).subjects.all.asc(:name).collect{|sub| [sub.name, sub.id]}
		end
	end

	def update
		profile = current_user.profile
		if current_user.is_employer?
			profile.update_attributes(employer_profile_params)
		elsif current_user.is_employee?
			profile.update_attributes(employee_profile_params)
		end
		if !current_user.basic_registeration_complete
			current_user.basic_registeration_complete = true
			current_user.save
		end
		profile.save
    if current_user.is_employee? && profile.resume.url.nil?
      redirect_to editresume_member_profile_path(current_user.user_name)
    else
		  redirect_to member_dashboards_path
    end
	end

	private

	def check_for_username
		username = params[:id]
		redirect_to login_path if current_user.user_name != username
	end

	def employer_profile_params
		params.require(:employer_profile).permit!
	end

	def employee_profile_params
		p = params.require(:employee_profile).permit!
		if params["new_skill_tags"].present?
			params["new_skill_tags"].each do |nst|
        if nst.length < 41
  				tag = SkillTag.create(:name => nst, :created_by => current_user.id)
  				if p["skill_tag_ids"].present?
  					p["skill_tag_ids"] << tag.id.to_s
  				else
  					p["skill_tag_ids"] = []
  					p["skill_tag_ids"] << tag.id.to_s
  				end
        end
			end
		end
		p
	end
	
end