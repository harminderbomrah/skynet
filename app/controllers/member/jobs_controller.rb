class Member::JobsController < SkynetMemberController

  before_action :check_for_employer, :only => ["applications", "application_info", "new"]
 
  def index
    @jobs = Job.where(:employer_profile_id => current_user.profile.id).desc(:created_at)
  end
  
  def show
    
  end

  def new
    @job = Job.new
    @job_categories = JobCategory.all.asc(:name)
    @job_sub_categories = @job_categories.first.job_sub_categories.all.asc(:name)
    @ug_qualifications = UgQualification.all.asc(:name)
    @subjects = @ug_qualifications.first.subjects.asc(:name)
    @pg_qualifications = PgQualification.all.asc(:name)
    @specializations = @pg_qualifications.first.specializations.asc(:name)
    @phd_qualifications = PhdQualification.all.asc(:name)
    @phd_specializations = @phd_qualifications.first.phd_specializations.asc(:name) rescue [] 
    @tag_array = []
    @tag_array_with_ids = {}
    SkillTag.each do |tag|
      @tag_array << tag.name
      @tag_array_with_ids[tag.name] = tag.id.to_s 
    end
  end

  def create
    @job = Job.new(job_params)
    @job[:employer_profile_id] = current_user.profile 
    if @job.save
       redirect_to  member_dashboards_path
     else
       redirect_to  new_member_job_path
    end
  end

  def edit
    @job = Job.where(:uid => params[:id]).first rescue nil
    @job_categories = JobCategory.all.asc(:name)
    @job_sub_categories = @job_categories.first.job_sub_categories.all.asc(:name)

    @job_sub_category = JobSubCategory.where(:job_category_id =>@job[:job_category_id], :id => @job[:job_sub_category_id]).first
    
    @ug_qualifications = UgQualification.all.asc(:name)
    @subjects = @ug_qualifications.first.subjects.asc(:name)
    
    @pg_qualifications = PgQualification.all.asc(:name)
    @specializations = @pg_qualifications.first.specializations.asc(:name)
     
    @phd_qualifications = PhdQualification.all.asc(:name)
    @phd_specializations = @phd_qualifications.first.phd_specializations.asc(:name) rescue []
    @tag_array = []
    @tag_array_with_ids = {}
    SkillTag.each do |tag|
      @tag_array << tag.name
      @tag_array_with_ids[tag.name] = tag.id.to_s 
    end
  end

  def update
    job = Job.find(params[:id])
    if !job.nil?
      if job.update_attributes(job_params)
        if params[:page]
          redirect_to member_jobs_path(:page => params[:page])
        else
          redirect_to member_jobs_path
        end
      else
        redirect_to edit_member_job(job_params)
      end
    end
  end

  def destroy
    job = Job.find(params[:id])rescue nil
    job.destroy if !job.nil?
    redirect_to member_dashboards_path(:page => params[:page])
  end

  def get_job_sub
    jc = JobCategory.find(params[:job_category]) rescue nil
    if !jc.nil?
     @job_sub_categories = jc.job_sub_categories.all.asc(:name)
    end
    render :layout => false
  end

  def set_review_status
    application = JobApplication.find(params[:id])
    application.reviewed = true
    application.save
    render :json => {"success" => true}.to_json
  end

  def get_specialization
    case params[:type]
    when "ug"
      ug = UgQualification.find(params[:id])
      @objs = ug.subjects.asc(:name) rescue []
    when "pg"
      pg = PgQualification.find(params[:id])
      @objs = pg.specializations.asc(:name) rescue []
    when "phd"
      phd = PhdQualification.find(params[:id])
      @objs = phd.phd_specializations.asc(:name) rescue []
    end
    render :layout => false
  end

  def applications
    @job_skill_tags = @job.skill_tags

    case params[:status]
    when "0"
      @applications = @job.job_applications.new_applications
    when "1"
      @applications = @job.job_applications.interviewed
    when "2"
      @applications = @job.job_applications.offer_extended
    when "3"
      @applications = @job.job_applications.hired
    when "4"
      @applications = @job.job_applications.archived
    else
      @applications = @job.job_applications.new_applications
    end
    case params[:sort]
    when "date"
      @applications = @applications.order_by(:created_at => :desc)
    when "name"
      @applications = @applications.order_by(:"employee_profile.name" => :asc)
    when "rating"
      @applications = @applications.order_by(:rating => :desc)
    else
      @applications = @applications.order_by(:created_at => :desc)
    end
  end


  def fill_status
    job = Job.find(params[:id]) rescue nil
    if !job.nil?
      if params[:status] == "notfilled"
        job.filled = true
      elsif params[:status] == "filled"
        job.filled = false
      end
      job.save
      data = {"success" => true, "status" => job.filled}
    else
      data = {"success" => false}
    end
    render :json => data.to_json
  end


  def application_info
    ja = JobApplication.find(params[:application_id])
    ja.status = params[:status].present? ? params[:status] : ja.status
    ja.note = params[:note].present? ? params[:note] : ja.note
    ja.rating = params[:rating].present? ? params[:rating] : ja.rating
    ja.save
    render :json => {"success" => true, "rating" => ja.rating_for_stars, "status" => ja.get_status}.to_json
  end

  def delete_job_application
    ja = JobApplication.find(params[:id])
    job = ja.job
    ja.destroy
    redirect_to applications_member_job_path(job.uid)
  end

  def job_letter
    @job = Job.find(params[:id]) rescue nil
    @user = User.find(params[:user]) rescue nil
    render :layout => false
  end

  def send_job_letter
    user = User.find(params[:user]) rescue nil
    if !user.nil?
      user.send_job_letter(params[:msg], current_user)
      data = {"success" => true}
    else
      data = {"success" => false}
    end
    render :json => data.to_json
  end

  private

  def job_params
    p = params.require(:job).permit!
    if params["new_skill_tags"].present?
      params["new_skill_tags"].each do |nst|
        if nst.length < 41
          tag = SkillTag.create(:name => nst, :created_by => current_user.id)
          if p["skill_tag_ids"].present?
            p["skill_tag_ids"] << tag.id.to_s
          else
            p["skill_tag_ids"] = []
            p["skill_tag_ids"] << tag.id.to_s
          end
        end
      end
    end
    p  
  end
end