class Member::CandidatesController < SkynetMemberController
	before_action :current_user, :except => [:show,:edit,:update,:destroy]

	def index
		@jobs = Job.desc(:created_at).page(params[:page]).per(15)
		#render :text => @jobs.to_json and return
	end

	def show
		@job = Job.find(params[:id])
		@job_category = JobCategory.find(:id => @job.job_category_id )
		@job_sub_category = JobSubCategory.find(:id => @job.job_sub_category_id )
		@company_name = EmployerProfile.find(:id => @job.employer_profile_id)
		@location = ImportantLocation.find(:id => @job.location_id)
		@job_designation = Designation.find(:id => @job.designation_id)
	end

end
