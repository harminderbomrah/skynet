class Admin::SettingsController < BackendController

  before_action :check_for_super_admin
# job category start

  def job_category_index
    @job_categories = JobCategory.all.asc(:name).page(params[:page]).per(15)
  end

  def new_job_category
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @category = JobCategory.new 
    @icons = JSON.parse(File.read(File.join(Rails.root,"public","icons.json")))
  end

  def edit_job_category
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @category = JobCategory.find(params[:id]) rescue nil
    @icons = JSON.parse(File.read(File.join(Rails.root,"public","icons.json")))
  end

  def create_job_category
    category = JobCategory.new(job_category_params)
    if category.save
      redirect_to job_category_index_admin_settings_path
    else
      session[:admin_update_fail] = category.errors.full_messages
      redirect_to new_job_category_admin_settings_path
    end
  end

  def delete_job_category
    category = JobCategory.find(params[:id]) rescue nil
    category.destroy if !category.nil?
     if params[:page]
        redirect_to job_category_index_admin_settings_path(:page => params[:page])
      else
         redirect_to job_category_index_admin_settings_path
      end
  end

  def update_job_category
    category = JobCategory.find(params[:id]) rescue nil
    if !category.nil? 
      if category.update_attributes(job_category_params)
        if params[:page]
          redirect_to job_category_index_admin_settings_path(:page => params[:page])
        else
           redirect_to job_category_index_admin_settings_path
        end
      else
        session[:admin_update_fail] = category.errors.full_messages
        redirect_to edit_job_category_admin_setting_path(cat, :page => params[:page])
      end
    end
  end
# job category end

# job sub category begin

  def job_sub_category_index
    if params[:parent_id].present?
      @job_sub_categories = JobSubCategory.where(:job_category_id => params[:parent_id]).asc(:name).page(params[:page]).per(15)
    else
      @job_sub_categories = JobSubCategory.all.asc(:name).page(params[:page]).per(15)
    end
  end

  def new_job_sub_category
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @job_categories = JobCategory.all.asc(:name).collect{|jc| [jc.name, jc.id]}
    @category = JobSubCategory.new 
  end

  def edit_job_sub_category
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @job_categories = JobCategory.all.asc(:name).collect{|jc| [jc.name, jc.id]}
    @category = JobSubCategory.find(params[:id]) rescue nil
  end

  def create_job_sub_category
    category = JobSubCategory.new(job_sub_category_params)
    if category.save
      redirect_to job_sub_category_index_admin_settings_path
    else
      session[:admin_update_fail] = category.errors.full_messages
      redirect_to new_job_sub_category_admin_settings_path
    end
  end

  def delete_job_sub_category
    category = JobSubCategory.find(params[:id]) rescue nil
    category.destroy if !category.nil?
    if params[:page]
      redirect_to job_sub_category_index_admin_settings_path(:page => params[:page])
    else
       redirect_to job_sub_category_index_admin_settings_path
    end
  end

  def update_job_sub_category
    category = JobSubCategory.find(params[:id]) rescue nil
    if !category.nil? 
      if category.update_attributes(job_sub_category_params)
        if params[:page]
          redirect_to job_sub_category_index_admin_settings_path(:page => params[:page])
        else
           redirect_to job_sub_category_index_admin_settings_path
        end
      else
        session[:admin_update_fail] = category.errors.full_messages
        redirect_to edit_job_sub_category_admin_setting_path(cat, :page => params[:page])
      end
    end
  end

  # job sub category end

  # ug qualifications begin

  def ug_qualification_index
    @ug_qualifications = UgQualification.all.asc(:name).page(params[:page]).per(15)
  end

  def new_ug_qualification
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @ug_qualification = UgQualification.new
  end

  def edit_ug_qualification
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @ug_qualification = UgQualification.find(params[:id])
  end

  def delete_ug_qualification
    ug_qualification = UgQualification.find(params[:id]) rescue nil
    ug_qualification.destroy if !ug_qualification.nil?
    if params[:page]
      redirect_to ug_qualification_index_admin_settings_path(:page => params[:page])
    else
       redirect_to ug_qualification_index_admin_settings_path
    end
  end

  def update_ug_qualification
    ug_qualification = UgQualification.find(params[:id]) rescue nil
    if !ug_qualification.nil?
      if ug_qualification.update_attributes(ug_qualification_params)
        if params[:page]
          redirect_to ug_qualification_index_admin_settings_path(:page => params[:page])
        else
          redirect_to ug_qualification_index_admin_settings_path
        end
      else
        session[:admin_update_fail] = ug_qualification.errors.full_messages
        redirect_to edit_ug_qualification_admin_setting_path(ug_qualification)
      end
    end
  end

  def create_ug_qualification
    ug_qualification = UgQualification.new(ug_qualification_params)
    if ug_qualification.save
      redirect_to ug_qualification_index_admin_settings_path
    else
      session[:admin_update_fail] = ug_qualification.errors.full_messages
      redirect_to new_ug_qualification_admin_settings_path
    end
  end

  def subjects
    @ug_qualification = UgQualification.find(params[:id])
    @subjects = @ug_qualification.subjects.asc(:name).page(params[:page]).per(15)
  end

  # ug qualifications end

  #pg qualifications begin 

  def pg_qualification_index
    @pg_qualifications = PgQualification.all.asc(:name).page(params[:page]).per(15)
  end

  def new_pg_qualification
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @pg_qualification = PgQualification.new
  end

  def edit_pg_qualification
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @pg_qualification = PgQualification.find(params[:id])
  end

  def delete_pg_qualification
    pg_qualification = PgQualification.find(params[:id]) rescue nil
    pg_qualification.destroy if !pg_qualification.nil?
    if params[:page]
      redirect_to pg_qualification_index_admin_settings_path(:page => params[:page])
    else
       redirect_to pg_qualification_index_admin_settings_path
    end
  end

  def update_pg_qualification
    pg_qualification = PgQualification.find(params[:id]) rescue nil
    if !pg_qualification.nil?
      if pg_qualification.update_attributes(pg_qualification_params)
        if params[:page]
          redirect_to pg_qualification_index_admin_settings_path(:page => params[:page])
        else
          redirect_to pg_qualification_index_admin_settings_path
        end
      else
        session[:admin_update_fail] = pg_qualification.errors.full_messages
        redirect_to edit_pg_qualification_admin_setting_path(pg_qualification)
      end
    end
  end

  def create_pg_qualification
    pg_qualification = PgQualification.new(pg_qualification_params)
    if pg_qualification.save
      redirect_to pg_qualification_index_admin_settings_path
    else
      session[:admin_update_fail] = pg_qualification.errors.full_messages
      redirect_to new_pg_qualification_admin_settings_path
    end
  end

  def specializations
    @pg_qualification = PgQualification.find(params[:id])
    @specializations = @pg_qualification.specializations.asc(:name).page(params[:page]).per(15)
  end

  # pg qualifications end

  # phd begin 

  def phd_qualification_index
    @phd_qualifications = PhdQualification.all.asc(:name).page(params[:page]).per(15)
  end

  def new_phd_qualification
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @phd_qualification = PhdQualification.new
  end

  def edit_phd_qualification
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @phd_qualification = PhdQualification.find(params[:id])
  end

  def delete_phd_qualification
    phd_qualification = PhdQualification.find(params[:id]) rescue nil
    phd_qualification.destroy if !phd_qualification.nil?
    if params[:page]
      redirect_to phd_qualification_index_admin_settings_path(:page => params[:page])
    else
       redirect_to phd_qualification_index_admin_settings_path
    end
  end

  def update_phd_qualification
    phd_qualification = PhdQualification.find(params[:id]) rescue nil
    if !phd_qualification.nil?
      if phd_qualification.update_attributes(phd_qualification_params)
        if params[:page]
          redirect_to phd_qualification_index_admin_settings_path(:page => params[:page])
        else
          redirect_to phd_qualification_index_admin_settings_path
        end
      else
        session[:admin_update_fail] = phd_qualification.errors.full_messages
        redirect_to edit_phd_qualification_admin_setting_path(phd_qualification)
      end
    end
  end

  def create_phd_qualification
    phd_qualification = PhdQualification.new(phd_qualification_params)
    if phd_qualification.save
      redirect_to phd_qualification_index_admin_settings_path
    else
      session[:admin_update_fail] = phd_qualification.errors.full_messages
      redirect_to new_phd_qualification_admin_settings_path
    end
  end

  def phd_specializations
    @phd_qualification = PhdQualification.find(params[:id])
    @specializations = @phd_qualification.phd_specializations.asc(:name).page(params[:page]).per(15)
  end

  # phd qualifications end

  # industry list begin

  def industry_index
    @industries = Industry.all.asc(:name).page(params[:page]).per(15)
  end

  def edit_industry
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @industry = Industry.find(params["id"])
  end

  def update_industry
    industry = Industry.find(params[:id])
    if !industry.nil?
      if industry.update_attributes(industry_params)
        if params[:page]
          redirect_to industry_index_admin_settings_path(:page => params[:page])
        else
          redirect_to industry_index_admin_settings_path
        end
      else
        session[:admin_update_fail] = industry.errors.full_messages
        redirect_to edit_industry_admin_setting_path(industry)
      end
    end
  end

  def new_industry
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @industry = Industry.new
  end

  def create_industry
    industry = Industry.new(industry_params)
    if industry.save
      redirect_to industry_index_admin_settings_path
    else
      session[:admin_update_fail] = industry.errors.full_messages
      redirect_to new_industry_admin_settings_path
    end
  end

  def delete_industry
    industry = Industry.find(params[:id]) rescue nil
    industry.destroy if !industry.nil?
    if params[:page]
      redirect_to industry_index_admin_settings_path(:page => params[:page])
    else
       redirect_to industry_index_admin_settings_path
    end
  end

# industry end

#country begin

  def country_index
    @countries = Country.all.asc(:name).page(params[:page]).per(15)
  end

  def edit_country
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @country = Country.find(params["id"])
  end

  def new_country
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @country = Country.new
  end

  def update_country
    country = Country.find(params[:id])
    if !country.nil?
      if country.update_attributes(country_params)
        if params[:page]
          redirect_to country_index_admin_settings_path(:page => params[:page])
        else
          redirect_to country_index_admin_settings_path
        end
      else
        session[:admin_update_fail] = country.errors.full_messages
        redirect_to edit_country_admin_setting_path(country)
      end
    end
  end

  def create_country
    country = Country.new(country_params)
    if country.save
      redirect_to country_index_admin_settings_path
    else
      session[:admin_update_fail] = country.errors.full_messages
      redirect_to new_country_admin_settings_path
    end
  end

  def delete_country
    country = Country.find(params[:id]) rescue nil
    country.destroy if !country.nil?
    if params[:page]
      redirect_to country_index_admin_settings_path(:page => params[:page])
    else
       redirect_to country_index_admin_settings_path
    end
  end

  # country end

  # location begin

  def location_index
    @locations = ImportantLocation.all.asc(:name).page(params[:page]).per(15)
  end

  def edit_location
     @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @location = ImportantLocation.find(params["id"])
  end

  def new_location
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @location = ImportantLocation.new
  end

  def update_location
    location = ImportantLocation.find(params[:id])
    if !location.nil?
      if location.update_attributes(location_params)
        if params[:page]
          redirect_to location_index_admin_settings_path(:page => params[:page])
        else
          redirect_to location_index_admin_settings_path
        end
      else
        session[:admin_update_fail] = location.errors.full_messages
        redirect_to edit_location_admin_setting_path(location)
      end
    end
  end

  def create_location
    location = ImportantLocation.new(location_params)
    if location.save
      redirect_to location_index_admin_settings_path
    else
      session[:admin_update_fail] = location.errors.full_messages
      redirect_to new_location_admin_settings_path
    end
  end

  def delete_location
    location = ImportantLocation.find(params[:id]) rescue nil
    location.destroy if !location.nil?
    if params[:page]
      redirect_to location_index_admin_settings_path(:page => params[:page])
    else
       redirect_to location_index_admin_settings_path
    end
  end

# location end

# designation begin

  def designation_index
    @designations = Designation.all.asc(:name).page(params[:page]).per(15)
  end

  def edit_designation
     @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @designation = Designation.find(params["id"])
  end

  def update_designation
    designation = Designation.find(params[:id])
    if !designation.nil?
      if designation.update_attributes(designation_params)
        if params[:page]
          redirect_to designation_index_admin_settings_path(:page => params[:page])
        else
          redirect_to designation_index_admin_settings_path
        end
      else
        session[:admin_update_fail] = designation.errors.full_messages
        redirect_to edit_designation_admin_setting_path(designation)
      end
    end
  end

  def new_designation
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @designation = Designation.new
  end

  def create_designation
    designation = Designation.new(designation_params)
    if designation.save
      redirect_to designation_index_admin_settings_path
    else
      session[:admin_update_fail] = designation.errors.full_messages
      redirect_to new_designation_admin_settings_path
    end
  end

  def delete_designation
    designation = Designation.find(params[:id]) rescue nil
    designation.destroy if !designation.nil?
    if params[:page]
      redirect_to designation_index_admin_settings_path(:page => params[:page])
    else
       redirect_to designation_index_admin_settings_path
    end
  end




  private

  def job_category_params
    params.require(:job_category).permit!
  end

  def job_sub_category_params
    params.require(:job_sub_category).permit!
  end

  def ug_qualification_params
    params.require(:ug_qualification).permit!
  end

  def pg_qualification_params
    params.require(:pg_qualification).permit!
  end

  def phd_qualification_params
    params.require(:phd_qualification).permit!
  end

  def industry_params
    params.require(:industry).permit!
  end

  def country_params
    params.require(:country).permit!
  end

  def location_params
    params.require(:important_location).permit!
  end

   def designation_params
    params.require(:designation).permit!
  end

end