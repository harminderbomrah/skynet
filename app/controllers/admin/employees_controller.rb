class Admin::EmployeesController < BackendController
  
  before_action :load_variables, :only => [:new, :edit]

  def index
    @employees = User.employees.desc(:created_at).page(params[:page]).per(15)
  end

  def new
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @user = User.create nil
    @profile = @user.build_employee_profile
    @industries = Industry.all.asc(:name).collect{|industry| [industry.name, industry.id]}
    @job_categories = JobCategory.all.asc(:name)
    @job_sub_categories = @job_categories.first.job_sub_categories.all.asc(:name)
  end

  def search
    @resumes = []
    sort_type = params[:sort].present? ? params[:sort] : "desc"
    @optimized = false
    if params[:keywords].present? || params[:years].present? || params[:location].present? || params[:skills].present?
      @resumes = EmployeeProfile.searchable
      @resumes = keyword_search(@resumes)
      @resumes = sort_data(@resumes)
      @resumes = @resumes.order_by(:updated_at => sort_type).page(params[:page]).per(10)
    elsif params[:name].present?
      pname = ".*" + params[:name] + ".*"
      regex = Regexp.new(pname, "i")
      @resumes = EmployeeProfile.where(:name => regex)
      @resumes = keyword_search(@resumes)
      @resumes = sort_data(@resumes)
      @resumes = @resumes.order_by(:updated_at => sort_type).page(params[:page]).per(10)
    end
  end

  def show
    @user = User.where(:user_name => params[:id]).first rescue nil
    @profile = @user.profile if !@user.nil?
    redirect_to admin_employees_path if @user.nil?
  end

  def edit
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @user = User.where(:user_name => params[:id]).first rescue nil
    @profile = @user.profile
    @industries = Industry.all.asc(:name).collect{|industry| [industry.name, industry.id]}
    @job_categories = JobCategory.all.asc(:name)
    if @profile.job_category.nil? || @profile.job_category.empty?
      @job_sub_categories = @job_categories.first.job_sub_categories.all.asc(:name)
    else
      @job_sub_categories = @job_categories.find(@profile.job_category).job_sub_categories.all.asc(:name)
    end
  end

  def getselectdata
    if params[:type] == "qualification"
      case params[:value]
      when "0"
        @qualifications = UgQualification.all.asc(:name).collect{|qual| [qual.name, qual.id]}
      when "1"
        @qualifications = PgQualification.all.asc(:name).collect{|qual| [qual.name, qual.id]}
      when "2"
        @qualifications = PhdQualification.all.asc(:name).collect{|qual| [qual.name, qual.id]}
      end
    elsif params[:type] == "specialization"
      case params[:qualification_type]
      when "0"
        @qualifications = UgQualification.find(params[:value]).subjects.asc(:name).collect{|qual| [qual.name, qual.id]}
      when "1"
        @qualifications = PgQualification.find(params[:value]).specializations.asc(:name).collect{|qual| [qual.name, qual.id]}
      when "2"
        @qualifications = PhdQualification.find(params[:value]).phd_specializations.asc(:name).collect{|qual| [qual.name, qual.id]}
      end
    end
    render :layout => false
  end

  def update
    user = User.where(:user_name => params[:id]).first rescue nil
    if !user.nil?
      user.update_attributes(employee_params)
      if user.save
        if params[:page]
          if params[:src]
            redirect_to search_admin_employees_path(:page => params[:page])
          else
            redirect_to admin_employees_path(:page => params[:page])
          end
        else
          if params[:src]
            redirect_to search_admin_employees_path
          else
            redirect_to admin_employees_path
          end
        end
      else
        session[:admin_update_fail] = user.errors.full_messages
        redirect_to edit_admin_employee_path(user.user_name, :page =>  params[:page])
      end
    end
  end

  def create
    user = User.new(employee_params)
    if user.save
      redirect_to admin_employees_path
    else
      session[:admin_update_fail] = user.errors.full_messages
      redirect_to new_admin_employee_path
    end
  end

  def destroy
    user = User.where(:user_name => params[:id]).first rescue nil
    user.destroy if !user.nil?
    if params[:src]
      redirect_to search_admin_employees_path(:page => params[:page])
    else
      redirect_to admin_employees_path(:page => params[:page])
    end 
  end

  private

  def keyword_search(data)
    if params[:keywords].present?
      key_string = params[:keywords]
      key_string = key_string.strip.nil? ? key_string : key_string.strip
      keywords = key_string.split(",")
      regex = Regexp.new(keywords.collect{|word| ".*" + word.strip + ".*"}.join(""), "i")
      data = data.where(:searchable_text => regex)
    end
    data
  end

  def sort_data(data)
    if params[:years].present?
      if params[:years].to_i > 0
        data = data.where(:years_of_exp.gte => params[:years].to_i)
      else
        data = data.where(:years_of_exp => params[:years].to_i)
      end
    end
    if params[:location].present?
      data = data.where(:location.in => params[:location])
    end
    if params[:qualification].present?
      data = data.where(:highest_qualification_type.gte => params[:qualification])
    end
    if params[:skills].present?
      data = data.where(:skill_tag_ids.in => params[:skills])
    end
    if params[:industry].present?
      data = data.where(:industry_id => params[:industry])
    end
    if params[:job_category].present?
      data = data.where(:job_category => params[:job_category])
    end
    if params[:job_sub_category].present?
      data = data.where(:job_sub_category => params[:job_sub_category])
    end
    data
  end


  def load_variables
    @tag_array = []
    @tag_array_with_ids = {}
    SkillTag.each do |tag|
      @tag_array << tag.name
      @tag_array_with_ids[tag.name] = tag.id.to_s 
    end
    @industries = Industry.all.collect{|industry| [industry.name, industry.id]}
    @states = Country.first.states.collect{|state| [state.name, state.id]}
    @locations = ImportantLocation.all.collect{|loc| [loc.name, loc.id]}
    @countries = Country.all.collect{|country| [country.name, country.id]}
    # @ug_qualifications = UgQualification.all.asc(:name).collect{|qual| [qual.name, qual.id]}
    # @ug_specializations = UgQualification.find(@ug_qualifications.first.last).subjects.all.asc(:name).collect{|sub| [sub.name, sub.id]}
  end

  def employee_params
    p = params.require(:user).permit!
    if params["new_skill_tags"].present?
      params["new_skill_tags"].each do |nst|
        if nst.length < 41
          tag = SkillTag.create(:name => nst, :created_by => current_user.id)
          if p["employee_profile_attributes"]["skill_tag_ids"].present?
            p["employee_profile_attributes"]["skill_tag_ids"] << tag.id.to_s
          else
            p["employee_profile_attributes"]["skill_tag_ids"] = []
            p["employee_profile_attributes"]["skill_tag_ids"] << tag.id.to_s
          end
        end
      end
    end
    p
  end

end