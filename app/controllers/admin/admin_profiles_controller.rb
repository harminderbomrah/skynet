class Admin::AdminProfilesController < BackendController

  before_action :check_for_super_admin, :except => [:edit,:update]

  def index
    @users = User.admins.desc(:created_at).page(params[:page]).per(15)
  end

  def new
    @user = User.new
    @user.build_admin_profile
  end

  def edit
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @user = User.where(:user_name => params[:id]).first rescue nil
    redirect_to admin_dashboards_url and return if !current_user.is_super_admin? && current_user.user_name != @user.user_name
    @profile = @user.profile
  end

  def create
    user = User.new(admin_params)
    if user.save
      redirect_to admin_admin_profiles_path
    else
      session[:admin_update_fail] = user.errors.full_messages
      redirect_to new_admin_admin_profile_path
    end
  end

  def update
    user = User.where(:user_name => params[:id]).first
    user.update_attributes(admin_params)
    if user.save
      if params[:page].present?
        redirect_to admin_admin_profiles_path(:page => params[:page])
      else
        redirect_to admin_admin_profiles_path
      end
    else
      session[:admin_update_fail] = user.errors.full_messages
      redirect_to edit_admin_admin_profile_path(user.user_name, :page => params[:page])
    end
  end

  def destroy
    user = User.where(:user_name => params[:id]).first rescue nil
    user.destroy if !user.nil?
    redirect_to admin_admin_profiles_path(:page => params[:page])
  end


  private

  def admin_params
    params.require(:user).permit!
  end

end