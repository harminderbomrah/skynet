class Admin::DashboardsController < BackendController
  def index
    @latestposts = Job.all.desc(:created_at).limit(5)
    @todo = TodoList.new
    @todolist = TodoList.all.desc(:created_at)
    @user_array_with_ids = {}
    @user_array = []
    User.admins.where(:id.ne => current_user.id ).each do |usr|
      @user_array_with_ids[usr.name] = usr.id.to_s
      @user_array << usr.name
    end
  end

  def show
  end

  def create
    todo = TodoList.create(todoparams)
    render :partial => "todo", :object => todo
  end

  def destroy
    todo = TodoList.find(params[:id])
    todo.destroy
    render :json => {"success" => true}.to_json
  end

  def get_chart_data
    datasets = {}
    new_users = []
    jobs_posted = []
    position_filled = []    
    (1..31).each do |idx|
      dt = Time.now - idx.days
      temp = dt.strftime("%Y/%m/%d")
      temp = temp + " 00:00"
      sdt = Time.parse(temp)
      edt = sdt + 1.days
      users = User.where(:created_at.gt => sdt, :created_at.lt => edt).count
      new_users << [sdt.to_i * 1000, users]
      jobs = Job.where(:created_at.gt => sdt, :created_at.lt => edt).count
      jobs_posted << [sdt.to_i * 1000, jobs]
      jobs = Job.where(:created_at.gt => sdt, :created_at.lt => edt, :filled => true).count
      position_filled << [sdt.to_i * 1000, jobs]
    end

    datasets["new_users"] = {
      "label" => "New User Registered",
      "data" => new_users
    }
    datasets["jobs_posted"] = {
      "label" => "Jobs Posted",
      "data" => jobs_posted
    }
    datasets["position_filled"] = {
      "label" => "Position Filled",
      "data" => position_filled
    }
    render :json => datasets.to_json
  end

  private 

  def todoparams
    params.require(:todo_list).permit!
  end

end