class Admin::EmployersController < BackendController

  before_action :load_variables, :only => [:new, :edit]

  def index
    @employers = User.employers.desc(:created_at).page(params[:page]).per(15)
  end

  def new
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @user = User.new
    @profile = @user.build_employer_profile
  end

  def show
    @user = User.where(:user_name => params[:id]).first rescue nil
    redirect_to admin_employers_path if @user.nil?
  end

  def create
    user = User.new(employer_params)
    if user.save
       redirect_to admin_employers_path
    else
      session[:admin_update_fail] = user.errors.full_messages
      redirect_to new_admin_employer_path
    end
  end

  def edit
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @user = User.where(:user_name => params[:id]).first rescue nil
    @profile = @user.profile
  end

  def update
    user = User.where(:user_name => params[:id]).first rescue nil
    if !user.nil?
      if user.update_attributes(employer_params)
        if params[:page]
          redirect_to admin_employers_path(:page => params[:page])
        else
          redirect_to admin_employers_path
        end
      else
        session[:admin_update_fail] = user.errors.full_messages
        redirect_to edit_admin_employer_path(user.user_name, :page =>  params[:page])
      end
    end
  end

  def destroy
    user = User.where(:user_name => params[:id]).first rescue nil
    user.destroy if !user.nil?
    redirect_to admin_employers_path(:page => params[:page])
  end

  private


  def load_variables
    @industries = Industry.all.collect{|industry| [industry.name, industry.id]}
    @states = Country.first.states.collect{|state| [state.name, state.id]}
    @locations = ImportantLocation.all.collect{|loc| [loc.name, loc.id]}
    @countries = Country.all.collect{|country| [country.name, country.id]}
  end

  def employer_params
    params.require(:user).permit!
  end

end