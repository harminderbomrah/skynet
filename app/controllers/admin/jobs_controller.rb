class Admin::JobsController < BackendController
  # before_action :check_for_super_admin, :except => [:show,:edit,:update,:destroy, :index]

  def index
    @jobs = Job.desc(:created_at).page(params[:page]).per(15)
  end

  def show
    @job = Job.find(params[:id])
    
    @job_emp = EmployerProfile.find(@job[:employer_profile_id])
    @job_designation = Designation.where(:id => @job[:designation_id]).first rescue nil

    @job_category = JobCategory.where(:id => @job[:job_category_id]).first rescue nil
    @job_sub_category = JobSubCategory.where(:job_category_id =>@job[:job_category_id], :id => @job[:job_sub_category_id]).first rescue nil

    @industry = Industry.where(:id => @job[:industry_id]).first rescue nil

    @ug_qualifications = UgQualification.where(:id => @job[:ug_qualification_id]).first rescue nil
    @subjects = Subject.where(:id => @job[:subject_id],:ug_qualification_id =>@job[:ug_qualification_id]).first rescue []
    
    @pg_qualifications = PgQualification.where(:id => @job[:pg_qualification_id]).first rescue nil
    @specializations = Specialization.where(:id => @job[:specialization_id],:pg_qualification_id =>@job[:pg_qualification_id]).first rescue []

    @phd_qualifications = PhdQualification.where(:id => @job[:phd_qualification_id]).first rescue nil
    @phd_specializations = PhdSpecialization.where(:id => @job[:phd_specialization_id],:phd_qualification_id =>@job[:phd_qualification_id]).first rescue []
    # render :text => @job_sub_category.to_json and return 

  end

  def new
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end
    @job = Job.new
    @job_categories = JobCategory.all.asc(:name)
    @job_sub_categories = @job_categories.first.job_sub_categories.all.asc(:name)
    @ug_qualifications = UgQualification.all.asc(:name)
    @subjects = @ug_qualifications.first.subjects.asc(:name)
    @pg_qualifications = PgQualification.all.asc(:name)
    @specializations = @pg_qualifications.first.specializations.asc(:name)
    @phd_qualifications = PhdQualification.all.asc(:name)
    @phd_specializations = @phd_qualifications.first.phd_specializations.asc(:name) rescue []
    @tag_array = []
    @tag_array_with_ids = {}
    SkillTag.each do |tag|
      @tag_array << tag.name
      @tag_array_with_ids[tag.name] = tag.id.to_s 
    end
  end

   

  def create
    job = Job.new(job_params)
    #render :text => job.to_json and return
    if job.save
       redirect_to admin_jobs_path
    else
      session[:admin_update_fail] = job.errors.full_messages
      redirect_to new_admin_job_path
    end
  end

  def edit
    @update_errors = []
    if session[:admin_update_fail]
      @update_errors = session[:admin_update_fail]
      session.delete(:admin_update_fail)
    end

    @job = Job.find(params[:id]) rescue nil

    @job_categories = JobCategory.all.asc(:name)
    @job_sub_categories = @job_categories.first.job_sub_categories.all.asc(:name)

    #@job_sub_category = JobSubCategory.where(:job_category_id =>@job[:job_category_id], :id => @job[:job_sub_category_id]).first
    
     ##render :text => @job_sub_category.to_json and return

    @ug_qualifications = UgQualification.all.asc(:name)
    @subjects = @ug_qualifications.first.subjects.asc(:name)
    
    @pg_qualifications = PgQualification.all.asc(:name)
    @specializations = @pg_qualifications.first.specializations.asc(:name)
     
    @phd_qualifications = PhdQualification.all.asc(:name)
    @phd_specializations = @phd_qualifications.first.phd_specializations.asc(:name) rescue []
    @tag_array = []
    @tag_array_with_ids = {}
    SkillTag.each do |tag|
      @tag_array << tag.name
      @tag_array_with_ids[tag.name] = tag.id.to_s 
    end
    
end
    
  def update
    job = Job.find(params[:id])
     #render :text => params.to_json and return
    if !job.nil?
      if job.update_attributes(job_params)
        if params[:page]
          redirect_to admin_jobs_path(:page => params[:page])
        else
          redirect_to admin_jobs_path
        end
      else
        session[:admin_update_fail] = job.errors.full_messages
        redirect_to edit_admin_job(job_params)
      end
    end
  end

  def destroy
    job = Job.find(params[:id]) rescue nil
    job.destroy if !job.nil?
    if params[:page]
      redirect_to admin_jobs_path(:page => params[:page])
    else
       redirect_to admin_jobs_path
    end
  end

  def get_job_subad
    
    jc = JobCategory.find(params[:job_category]) rescue nil
    if !jc.nil?
     @job_sub_categories = jc.job_sub_categories.all.asc(:name)
    end
    render :layout => false
  end

  def get_specializationad
    case params[:type]
    when "ug"
      ug = UgQualification.find(params[:id])
      @objs = ug.subjects.asc(:name) rescue []
    when "pg"
      pg = PgQualification.find(params[:id])
      @objs = pg.specializations.asc(:name) rescue []
    when "phd"
      phd = PhdQualification.find(params[:id])
      @objs = phd.phd_specializations.asc(:name) rescue []
    end
    render :layout => false
  end

  private
  def job_params
    p = params.require(:job).permit!
    if params["new_skill_tags"].present?
      params["new_skill_tags"].each do |nst|
        tag = SkillTag.create(:name => nst, :created_by => current_user.id)
        if p["skill_tag_ids"].present?
          p["skill_tag_ids"] << tag.id.to_s
        else
          p["skill_tag_ids"] = []
          p["skill_tag_ids"] << tag.id.to_s
        end
      end
    end
    p  
  end
end