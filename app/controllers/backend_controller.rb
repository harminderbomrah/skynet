class BackendController < ApplicationController
  layout "admin"
  before_action :check_for_admin

  private 

  def check_for_admin
    redirect_to root_url and return if current_user.nil? || !current_user.is_admin?
  end

  def check_for_super_admin
    redirect_to admin_dashboards_url and return if current_user.nil? || !current_user.is_super_admin?
  end

end