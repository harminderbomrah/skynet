class ConfirmationMailer <  ActionMailer::Base
	default :from => "noreply@skynetplacements.com"
	def setup
	    mail_setting = {
		    address:              'smtp.gmail.com',
		    port:                 587,
		    domain:               'www.skynetplacements.com',
		    user_name:            'reach@skynetplacements.com',
		    password:             'Sky@1234',
		    authentication:       'plain',
		    enable_starttls_auto: true  
		}
		
	    ActionMailer::Base.smtp_settings = mail_setting
	end
	
	def send_confirmation_mail(user, host)
		setup
		@user = user
		@url = "http://" + host + "/users/confirm?id=#{user.confirmation_token}"
		mail(:to => user.email, :subject => "Confirmation Email")
	end

	def send_reset_mail(user, host)
		setup
		@user = user
		@url = "http://" + host + "/users/resetp?token=#{user.reset_token}"
		mail(:to => user.email, :subject => "Password reset Email")
	end

	def send_contact_us_mail(uname,email,contant)
		render :text => "Mail Send" and return
		# setup
		# @user = user
		# @url = "http://" + host + "/users/confirm?id=#{user.confirmation_token}"
		# mail(:to => user.email, :subject => "Confirmation Email")
	end

	def job_letter(html, employer, employee)
		setup
		@html = html
		mail(:to => employee.email, :subject => "Job Opportunity from #{employer.profile.company_name}")
	end
end
